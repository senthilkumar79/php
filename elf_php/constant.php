<?php 
$course_class =array('UKG - LKG','1 - 2','3 - 5');
$class_selection=array('LKG-UKG'=>array('LKG','UKG'),'1-2'=>array(1,2),'3-5'=>array(3,4,5));
$course_url_mapping=array('1'=>'course_lkgukg.php','2'=>'speakearly_lkg_ukg.php','3'=>'starreader_class_1_2.php','4'=>'speakup_class_1to2.php','5'=>'fastfluency_class_3to5.php');

$trial_course_url =array('1'=>'learntoread_booktrialform.php','2'=>'speakearly_booktrialform.php','3'=>'startreader_booktrialform.php','4'=>'speakup_booktrialform.php','5'=>'fastfluency_booktrialform.php');
?>