<?php
	include_once('dbconnect.php');
	$con= new db();
	$msg = null;  
	// print_r($_GET);
 	$decoded = base64_decode($_GET['link']);    
  	if(strrpos($decoded,'@') !== false)
        $decoded_details = explode("@",$decoded);        
    if(strrpos($decoded_details[0],'##') !== false)
        $decoded_array = explode("##",$decoded_details[0]);

	$url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	$isvalid = $con->validateURL($url,$decoded_array);		
	$batch_details=array();
	// print_r($isvalid);
	if(!empty($isvalid)){		
			$batch_details = $isvalid['batches'];			
	}      
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <title>ELF - Interactive LIVE Classes for  English Learnig</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="/ELF_PHONICS_LOGO.ico" type="image/icon" sizes="16x16">
  <!-- font -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Livvic:wght@600&display=swap" rel="stylesheet">
  <!-- css start -->
  <link rel="stylesheet" type="text/css" href="/webroot/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="/webroot/css/main.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet">
  <!-- css start -->
  <link rel="stylesheet" type="text/css" href="/webroot/css/style.css">
  <link rel="stylesheet" type="text/css" href="/webroot/css/responsive.css">
  <!-- js start -->
  <script type="text/javascript" src="/webroot/js/jquery-3.5.1.js"></script>
  <script type="text/javascript" src="/webroot/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="/webroot/js/script.js"></script>
</head>
<body>
	<!-- header start -->	
<?php include_once('constant.php');?>
<div id="header">
	<?php include_once('header.php');?>
</div>
	<!-- header ends here and Parents Chat  Start-->
	 <div class="clearfix">&nbsp;</div>
<div class="container">
  <div class="col-md-12">
    <div class="col-md-10 offset-md-1 main_elf"  style="min-height: 500px">
     <?php if($isvalid['status'] ==200  && !empty($batch_details)){?>
     	
		      <div class="goto_back1">
		        <!-- <a href="course_lkgukg.html" class=""> <i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i></a> -->
		      	<div class="home_newphone">
		          <a href="tel:+919176692924">
		        	<img src="/webroot/img/icon.png" alt="whatsapp-logo" class="whatsapp-logo"> <span>+91 96001 86000</span></a>
		      	</div>
		      </div>
			      <div class="elf_details text-center">
			            <img src="/webroot/img/elf_logo.png" alt="elf_logo">
			        <h2><span class="learn_elf"><?php echo $batch_details['course_name'];?></span>
			            <!-- <span class="learn_elf1">Phonics</span>--></h2>
			      <h4>Please enter your details</h4>
			    </div>
			<form id="bs-validate-demo" action="" method="post" onsubmit="return register(this,'elf_tr_purchase');">
			   <div class="row">
			   	<input type="hidden" name="product_type" value="1" />
			   	<input type="hidden" name="type" value="1" />
			  <div class="col-md-4 form-group">
			    <label for="NameDemo1">Parent's Name<span class='mandatory'>*</span></label>
			    <input type="text" name="parent_name" class="form-control col-md-12" pattern="^[a-zA-Z. ]+$" maxlength="25" required>
			        <div class="invalid-feedback">
			          Please enter Parent's Name.
			        </div>
			  </div>
			   <div class="col-md-4 form-group">
			    <label for="EmailDemo">Email<span class='mandatory'>*</span></label>
			    <input type="email" name="parent_email" class="form-control col-md-12" pattern="^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$" id="EmailDemo" required>
			      <div class="invalid-feedback">
			        Please Enter a valid email address!
			      </div>  
			  </div>

			  <div class="form-group col-md-4">
			    <label for="NameDemo2">Mobile Number<span class='mandatory'>*</span></label>
			   <input id="edit1" name="parent_mobile" pattern="[1-9]{1}[0-9]{9}" class="form-control col-md-12" maxlength="10" minlength="10" required="" type="text">
			      <div class="invalid-feedback">
			        Please enter a valid mobile number!
			      </div>    
			  </div>  
			  <div class="form-group col-md-6">
			    <label for="childDemo">Child’s Name</label>
			   <input id="child" name="student_name" placeholder="" class="form-control"  type="text">   
			  </div>
			    <div class="col-md-6 form-group">
			    <label for="selectDemo">Your child's Class<span class='mandatory'>*</span></label>
			    <select class="form-control col-md-6" id="slct1" name="student_class" required="">
			      <option value="">Select Child's Class</option>
			      <?php foreach ($class_selection[str_replace(' ','',$batch_details['class'])] as $key => $value) {?>
			      	<option value="<?php echo $value;?>"><?php echo $value;?></option>	
			      <?php }?>
			    </select>
			     <div class="invalid-feedback">
			         Please select your child class.
			      </div>
			  </div>
			    <div class="col-md-12 table-responsive-lg">
			      <label>Select a batch that suits you<span class='mandatory'>*</span></label>
			        <table id="example" class="table" style="width:100%">
			    <thead>
			      <tr id='head'>
			          <th scope="Row">Select</th>
			          <th>Batch No</th>
			          <th>Start Date</th>
			          <th>Days & Time</th>
			      </tr>
			  </thead>
			  <tbody>
			  	<?php foreach($batch_details['batches'] as $key=>$batches){?>
			  	<tr id='<?php echo $batches['id'];?>'>
			  		<td scope="col">
			            <input name="course_batch_id" id="batch<?php echo $batches['id'];?>" type="radio" value="<?php echo $batches['id'].'_'.$batches['reference_id'];?>" required>
			        </td>
			        <td><?php echo $batches['batch_id'];?></td>
			        <td><?php echo $batches['batch_start_date'];?></td>
			        <td><?php echo $batches['schedule_day'].' '.$batches['schedule_time'];?></td>
			        <td class="slots_td"><?php echo ($batches['max_batch_size']-$batches['total_registration']);?> Slots left</td>
			  	</tr>
			  <?php }?>
			    <!-- <tr id='one'>
			        <th scope="col">
			            <input name="UserSelected" id="userSelected_1" type="radio" value="1">
			        </th>
			        <td>ELR B1</td>
			        <td>05 Apr 2021</td>
			        <td>Mon, Thu 4 – 5 pm</td>
			        <td class="slots_td">4 Slots left</td>
			    </tr>
			    <tr id='two'>
			        <th scope="col">                          
			            <input name="UserSelected" id="userSelected_2" type="radio" value="3">
			        </th>
			        <td>ELR B2</td>
			        <td>06 Apr 2021</td>
			        <td>Mon, Thu 4 – 5 pm</td>
			        <td class="slots_td">3 Slots left</td>
			    </tr> -->
			</tbody>
			        </table>
			      </div>
			  <!-- ends here -->
			 <div class="col-md-10 text-center">
			 <button type="submit" class="btn btn-info book_btn new_paybtn">Register</button>
			    <a href="/" class="btn btn-info book_contiunue">Cancel</a>

			  </div>
			  </div>
			</form>
		
		<?php }else{?>
			<div class="col-md-12 text-center mt-5">
				<h2>OOPs!! Invalid Link...</h2>
			</div>
		<?php }?>	
		</div>
</div>
</div>

<div class="modal" id="thank-you" data-backdrop='static'>
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <!-- <h4 class="modal-title">Thank You</h4> -->
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <h3>Thank you</h3>
        <h3>You have been registered for the Course.</h3>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <a href='/' class="btn btn-danger">Close</a>
      </div>

    </div>
  </div>
</div>

  <a href="https://wa.me/+91 96001 86000" class="whatsapp_float whats-app" target="_blank">
    <i class="fa fa-whatsapp whatsapp-icon my-float"></i>
  </a>
  <!-- footer start -->
  <div id="footer"><?php include_once('footer.php');?></div>
<script>
(function() {
  'use strict';

  // window.addEventListener('load', function() {
  //   var form = document.getElementById('bs-validate-demo');
  //   form.addEventListener('submit', function(event) {
  //     if (form.checkValidity() === false) {
  //       event.preventDefault();
  //       event.stopPropagation();
  //     }
  //     form.classList.add('was-validated');
  //   }, false);
  // }, false);
})();
</script>

	 
<script type="text/javascript">
  $(document).ready(function () {
  $('#tableSelect tr').click(function() {
    $(this).find('th input:radio').prop('checked', true);
});
});
</script>
</body>
</html>