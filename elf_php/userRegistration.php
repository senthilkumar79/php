<?php
if(!isset($_SESSION))
    session_start();
  include_once('dbconnect.php');
  $con= new db();

  if(isset($_POST['tablename'])){
      if(isset($_POST['trial_batch_id'])){
          $ids = explode('_', $_POST['trial_batch_id']);
          $_POST['trial_class_id'] = $ids[1];
          $_POST['batch_id'] = $ids[0];
          unset($_POST['trial_batch_id']);
      }
      else if(isset($_POST['course_batch_id'])){
          $ids = explode('_', $_POST['course_batch_id']);
          $_POST['product_id'] = $ids[1];
          $_POST['batch_id'] = $ids[0];
          unset($_POST['course_batch_id']);
      }
      $data = $con->saveRegistration($_POST);
  	  if(!empty($data)){
  	  	 header("Content-Type:application/json");      	  
			  echo json_encode($data);			      
		}
	}
?>
