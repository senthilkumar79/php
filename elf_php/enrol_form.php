<!DOCTYPE html>
<html lang="en">
<head>
   <title>ELF - Interactive LIVE Classes for  English Learnig</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="../ELF_PHONICS_LOGO.ico" type="image/icon" sizes="16x16">
  <!-- font -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Livvic:wght@600&display=swap" rel="stylesheet">
  <!-- css start -->
  <link rel="stylesheet" type="text/css" href="webroot/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="webroot/css/main.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet">
  <!-- css start -->
  <link rel="stylesheet" type="text/css" href="webroot/css/style.css">
  <link rel="stylesheet" type="text/css" href="webroot/css/responsive.css">
  <!-- js start -->
  <script type="text/javascript" src="webroot/js/jquery-3.5.1.js"></script>
  <script type="text/javascript" src="webroot/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="webroot/js/script.js"></script>
</head>
<body>
	<!-- header start -->
<div id="header"><?php include('header.php');?></div>
	<!-- header ends here and Parents Chat  Start-->
	 <div class="clearfix">&nbsp;</div>
<div class="container">
  <div class="col-md-12">
    <div class="col-md-10 offset-md-1 main_elf">
      <div class="goto_back1">
        <a href="/" class=""> <i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i></a>
      <div class="home_newphone">
          <a href="tel:+919176692924">
        <img src="webroot/img/icon.png" alt="whatsapp-logo" class="whatsapp-logo"> <span>+91 96001 86000</span></a>
      </div>
      </div>
      <div class="elf_details text-center">
            <img src="webroot/img/elf_logo.png" alt="elf_logo">
        <h2><span class="learn_elf">Learn to Read</span>
            <span class="learn_elf1">Phonics</span></h2>
      <h4>Please enter your details</h4>
    </div>
<form id="bs-validate-demo" novalidate>
   <div class="row">
  <div class="col-md-4 form-group">
    <label for="NameDemo1">Parent's Name*</label>
    <input type="text" class="form-control col-md-12" maxlength="25" required>
        <div class="invalid-feedback">
          Please enter Parent's Name.
        </div>
  </div>
   <div class="col-md-4 form-group">
    <label for="EmailDemo">Email*</label>
    <input type="email" class="form-control col-md-12" id="EmailDemo" required>
      <div class="invalid-feedback">
        Please Enter a valid email address!
      </div>  
  </div>

  <div class="form-group col-md-4">
    <label for="NameDemo2">Mobile Number*</label>
   <input id="edit1" name="mobile_number" pattern="[1-9]{1}[0-9]{9}" class="form-control col-md-12" maxlength="10" minlength="10" required="" type="text">
      <div class="invalid-feedback">
         Please enter mobile number.
      </div>    
  </div>  
  <div class="form-group col-md-6">
    <label for="childDemo">Child’s Name</label>
   <input id="child" name="child_name" placeholder="" class="form-control"  type="text">   
  </div>
   <div class="form-group col-md-6 offset-md-0">
    <label for="childClass">Child’s Class</label>
   <input id="childclass" name="child_class" placeholder="" class="form-control"  type="text">
  </div>
    <div class="col-md-12 table-responsive-lg">
      <label>Select a batch that suits you</label>
        <table id="example" class="table" style="width:100%">
    <thead>
      <tr id='head'>
          <th scope="Row">Select</th>
          <th>Batch No</th>
          <th>Start Date</th>
          <th>Days & Time</th>
      </tr>
  </thead>
  <tbody>
    <tr id='one'>
        <th scope="col">
            <input name="UserSelected" id="userSelected_1" type="radio" value="1">
        </th>
        <td>ELR B1</td>
        <td>05 Apr 2021</td>
        <td>Mon, Thu 4 – 5 pm</td>
        <td class="slots_td">4 Slots left</td>
    </tr>
    <tr id='two'>
        <th scope="col">                          
            <input name="UserSelected" id="userSelected_2" type="radio" value="3">
        </th>
        <td>ELR B2</td>
        <td>06 Apr 2021</td>
        <td>Mon, Thu 4 – 5 pm</td>
        <td class="slots_td">3 Slots left</td>
    </tr>
</tbody>
        </table>
      </div>
  <!-- ends here -->
  <div class="col-md-12 text-center">
  <button type="submit" class="btn btn-info book_btn">Register</button>
  <!-- <a href="course_lkgukg.php" class="btn btn-info book_contiunue">CANCEL</a> -->
</div>
</div>
</form>
</div>
</div>
</div>
  <a href="https://wa.me/+91 96001 86000" class="whatsapp_float whats-app" target="_blank">
    <i class="fa fa-whatsapp whatsapp-icon my-float"></i>
  </a>
  <!-- footer start -->
  <div id="footer"><?php include('footer.php');?></div>
<script>
(function() {
  'use strict';

  window.addEventListener('load', function() {
    var form = document.getElementById('bs-validate-demo');
    form.addEventListener('submit', function(event) {
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add('was-validated');
    }, false);
  }, false);
})();
</script>

	 <div class="clearfix">&nbsp;</div>
<script type="text/javascript">
  $(document).ready(function () {
  $('#tableSelect tr').click(function() {
    $(this).find('th input:radio').prop('checked', true);
});
});
</script>
</body>
</html>