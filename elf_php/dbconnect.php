<?php 
define('host','localhost');
define('username','root');
define('password','');
define('dbname','elf');
class db {

	public  function __construct() {
        $this->db=new mysqli(host,username,password,dbname);
        $link = mysqli_connect(host,username,password,dbname);
		if(mysqli_connect_errno()){

				 echo 'connection failed';
				 die("Connection failed: " . $conn->connect_error);
		}
		else 
		{
			if(!isset($_SESSION))
				session_start();
			// echo 'connected';
		}

	}


	public  function getAllData($table_name) {
			$res='';$alldata=array();$i=0;
	     if($table_name == 'elf_ma_course' || $table_name == 'elf_ma_event')			
	      	$res= "SELECT * FROM `".$table_name."` where deleted=0";
	     else if($table_name == 'elf_ma_batch')			
	      	$alldata= $this->getallBatches($table_name);	      	
	     else if($table_name == 'elf_ma_trial_class')			
	      	$res= "SELECT elf_ma_trial_class.id,trial_class_id,elf_ma_course.course_id,elf_ma_course.course_name,elf_ma_course.class,elf_ma_trial_class.registration_link FROM ".$table_name.",elf_ma_course where elf_ma_course.id = elf_ma_trial_class.course_id and elf_ma_trial_class.deleted =0 and elf_ma_course.deleted=0";	      
	      if($res !=''){
		  	$result = mysqli_query($this->db,$res);                         	 			  
		  	if(mysqli_num_rows($result) > 0)
			{		
				while($data=mysqli_fetch_assoc($result)){						
					$alldata[$i] = $data;
					$i++;			
				 }						
			}
		}
		  
		  return array('status'=>200,'data'=>$alldata);
	}

	public  function getallBatches($table_name) {	     
	      	$res= "SELECT * FROM `".$table_name."` where deleted=0";	     
	      	$alldata=array();$i=0;
		  	$result = mysqli_query($this->db,$res);                         	 			  
		  	if(mysqli_num_rows($result) > 0)
			{		
				while($data=mysqli_fetch_assoc($result)){						
					if($data['category'] ==1){ //trail class
						$row = mysqli_query($this->db,"select * from elf_ma_trial_class where id=".$data['reference_id']." and deleted = 0");          
						if(mysqli_num_rows($row) > 0){
							$trial_class = mysqli_fetch_assoc($row);
							$crow = mysqli_query($this->db,"select * from elf_ma_course where id=".$trial_class['course_id']." and deleted = 0");          
							if(mysqli_num_rows($crow) > 0){
								$trial_class_course = mysqli_fetch_assoc($crow);
								$data['reference_name'] = $trial_class_course['course_name'];
								$data['reference_category'] = $trial_class['trial_class_id'];
								$data['category_name'] = 'Trial Class';
							}
							else
								$data=array();
						}
					}
					else if($data['category'] ==2){ //course
						$crow = mysqli_query($this->db,"select * from elf_ma_course where id=".$data['reference_id']." and deleted = 0");          
							if(mysqli_num_rows($crow) > 0){
								$course = mysqli_fetch_assoc($crow);
								$data['reference_name'] = $course['course_name'];
								$data['reference_category'] = $course['course_id'];
								$data['category_name'] = 'Course';
							}else
								$data=array();
					}
					else if($data['category'] ==3){ // event
						$crow = mysqli_query($this->db,"select * from elf_ma_event where id=".$data['reference_id']." and deleted = 0");          
							if(mysqli_num_rows($crow) > 0){
								$event = mysqli_fetch_assoc($crow);
								$data['reference_name'] = $event['event_name'];
								$data['reference_category'] = $event['event_id'];
								$data['category_name'] = 'Event';
							}else
								$data=array();
					}					
					if(!empty($data)){
						$alldata[$i] = $data;
						$i++;			
					}
				 }						
			}
		  
		  return $alldata;
	}

	public function saveData($data){		
			    $fields = $values = array();     
			 	$table = $data['table'];
			 	unset($data['table']);
		     foreach( array_keys($data) as $key ) {		        
		            $fields[] = "`$key`";
		            $values[] = "'" . mysqli_real_escape_string($this->db,$data[$key]) . "'";		        
		    }      
		    $fields = implode(",", $fields);
		    $values = implode(",", $values);
		    if( mysqli_query($this->db,"INSERT INTO `$table` ($fields) VALUES ($values)") ) {
		    		
			        return array( "mysql_error" => false,
			        			   "status" =>200,
			        			   "table_name" => $table			                      
			                    );
		    } else {
		        return array("status"=>500,"table_name"=>$table,"mysql_error" => mysqli_error($this->db),'error_msg'=>"'OOPs!! Something went wrong'");
		    }
	}


	public function updateData($table,$data,$id) {		
		$valueSets = array();		
		foreach($data as $key => $value) {
			if($value !='')
		   		$valueSets[] = $key . " = '" . $value . "'";
		}
		$valueSets[] ="modified = '".Date('Y-m-d H:i:s')."'";
		$conditionSets = array();	
		foreach($id as $key => $value) {
		   $conditionSets[] = $key . " = '" . $value . "'";
		}

		$sql = "UPDATE $table SET ". join(",",$valueSets) . " WHERE " . join(" AND ", $conditionSets);	    
	    if( mysqli_query($this->db,$sql) ) {    	
	        return array( "mysql_error" => false,
				        			   "status" =>200,
				        			   "table_name" => $table			                      
				                    );
	    } else {
	     return array("status"=>500,"table_name"=>$table,"mysql_error" => mysqli_error($this->db),'error_msg'=>"'OOPs!! Something went wrong'");
	    }
	}




	public function login($username,$password){
		$res=("SELECT * FROM `elf_ma_admin` WHERE email='".$username."' and    password='".$password."'");	
		$result = mysqli_query($this->db,$res);		
		$_SESSION =array();
		if(mysqli_num_rows($result) > 0)
		{		
			$row = mysqli_fetch_assoc($result);						
			$_SESSION['userData'] = $row;
			$_SESSION['userData']['login'] = true;
			return $row; 
		}else 
		{
		  return FALSE;
		}
	}

	public function validateURL($url,$array){ //id,category,type
		$query ='';
		switch ($array[1]) {
			case 1:
				$query = "SELECT elf_ma_trial_class.id,trial_class_id,elf_ma_course.course_id,elf_ma_course.course_name,elf_ma_course.class,elf_ma_trial_class.registration_link FROM elf_ma_trial_class,elf_ma_course where elf_ma_course.id = elf_ma_trial_class.course_id and elf_ma_trial_class.registration_link='".$url."' and elf_ma_trial_class.id=".$array[0]." and elf_ma_trial_class.deleted =0 and elf_ma_course.deleted=0";
				//"select * from `elf_ma_trial_class` where registration_link='".$url."' and id=".$array[0]." and deleted =0";					
				break;	
			case 2:
				if($array[2] ==2)
					$query = "select * from `elf_ma_course` where part_payment_link='".$url."' and id=".$array[0]." and deleted =0";				
				else if($array[2] ==1)		
				$query = "select * from `elf_ma_course` where payment_link='".$url."' and id=".$array[0]." and deleted =0";	
				break;
			case 3:
				$query = "select * from `elf_ma_event` where payment_link='".$url."' and id=".$array[0]." and deleted =0";		
				break;	
		}
		
		if($query !=''){
			$row =array();
			$result = mysqli_query($this->db,$query);
			if(mysqli_num_rows($result) > 0)
			{		
				$row = mysqli_fetch_assoc($result);		
				$row['batches'] = $this->getallBatchbyId($row['id'],$array['1']);	
				return array( "mysql_error" => false,"status" =>200,"batches" =>$row);
			}
			else 
			{	
			  return array( "mysql_error" => false,"status" =>404,"batches" =>$row);
			}

		}

	}

    public function getallBatchbyId($reference_id,$category){    	
    		$res= "SELECT * FROM `elf_ma_batch` where deleted=0 and category = ".$category." and reference_id=".$reference_id;	     
	      	$allbatches=array();$i=0;
		  	$result = mysqli_query($this->db,$res);                         	 			  
		  	if(mysqli_num_rows($result) > 0)
			{		
				while($data=mysqli_fetch_assoc($result)){	
					//get total regiatrationm
					$query = "SELECT count(*) FROM `elf_tr_purchase` where batch_id=".$data['id']." and order_status='success'";
					$reg_count = mysqli_query($this->db,$query);
					if(mysqli_num_rows($reg_count) >0)
						$data['total_registration'] = mysqli_fetch_assoc($reg_count)['count(*)'];
					else
						$data['total_registration'] =0;
					//get total regiatrationm
					$allbatches[]= $data;					
				 }						
			}
		  
		  return $allbatches;
    }

    public function saveRegistration($data){
    	$valueSets = array();		
    	$table = $data['tablename'];
		unset($data['tablename']);
		foreach( array_keys($data) as $key ) {		        
		            $fields[] = "`$key`";
		            $values[] = "'" . mysqli_real_escape_string($this->db,$data[$key]) . "'";		        
		    }      
		    $fields = implode(",", $fields);
		    $values = implode(",", $values);
		    if( mysqli_query($this->db,"INSERT INTO `$table` ($fields) VALUES ($values)") ) {
		    		
			        return array( "mysql_error" => false,
			        			   "status" =>200,
			        			   "table_name" => $table			                      
			                    );
		    } else {
		        return array("status"=>500,"table_name"=>$table,"mysql_error" => mysqli_error($this->db),'error_msg'=>"'OOPs!! Something went wrong'");
		    }
    }

    public function getReport($table_name,$type){
    	$res='';$alldata=array();$i=0;
    	// error_log($table_name.' '.$type);
    	if($type ==0 && $table_name=='elf_tr_trial_class_booking')
    		$res='select elf_tr_trial_class_booking.id as id,elf_ma_trial_class.trial_class_id as trial_class,elf_ma_batch.batch_id as batch_name,elf_ma_course.course_id as course_id,elf_ma_course.course_name as course_name,parent_name,parent_email,parent_mobile,student_class,student_reading_level,student_speaking_level,if(type=1,"Online","Offline") as type,elf_tr_trial_class_booking.created as reg_date,elf_ma_batch.schedule_day as schedule_day,elf_ma_batch.schedule_time as schedule_time from elf_tr_trial_class_booking,elf_ma_course,elf_ma_batch,elf_ma_trial_class where elf_ma_trial_class.id = elf_tr_trial_class_booking.trial_class_id and elf_ma_course.id = elf_ma_trial_class.course_id and elf_ma_batch.id = elf_tr_trial_class_booking.batch_id and elf_tr_trial_class_booking.deleted=0 and elf_ma_course.deleted=0 and elf_ma_batch.deleted=0 and elf_ma_trial_class.deleted=0';
    	else if($type ==0 && $table_name=='elf_tr_trial_booking')
    		$res = 'select elf_tr_trial_booking.id as id,elf_ma_course.course_id as course_id,elf_ma_course.course_name as course_name,parent_name,parent_email,parent_mobile,student_class,elf_tr_trial_booking.created as reg_date from elf_tr_trial_booking,elf_ma_course where elf_ma_course.id = elf_tr_trial_booking.student_prefered_course and elf_tr_trial_booking.deleted=0 and elf_ma_course.deleted=0';
    	else if($type > 0 && $table_name=='elf_tr_purchase'){
    		if($type == 1)
    				$res='select elf_tr_purchase.id as id,elf_ma_batch.batch_id as batch_name,elf_ma_course.course_id as product_id,elf_ma_course.course_name as product_name,parent_name,parent_email,parent_mobile,student_class,student_name,if(type=1,"Online","Offline") as type,elf_tr_purchase.created as reg_date,elf_ma_batch.schedule_day as schedule_day,elf_ma_batch.schedule_time as schedule_time from elf_tr_purchase,elf_ma_course,elf_ma_batch where  elf_ma_course.id = elf_ma_batch.reference_id and elf_ma_batch.id = elf_tr_purchase.batch_id and elf_tr_purchase.deleted=0 and elf_ma_course.deleted=0 and elf_ma_batch.deleted=0 and elf_tr_purchase.product_type='.$type;
    		else
    			$res='select elf_tr_purchase.id as id,elf_ma_batch.batch_id as batch_name,elf_ma_event.event_id as product_id,elf_ma_event.event_name as product_name,parent_name,parent_email,parent_mobile,student_class,student_name,if(type=1,"Online","Offline") as type,elf_tr_purchase.created as reg_date,elf_ma_batch.schedule_day as schedule_day,elf_ma_batch.schedule_time as schedule_time from elf_tr_purchase,elf_ma_event,elf_ma_batch where  elf_ma_event.id = elf_ma_batch.reference_id and elf_ma_batch.id = elf_tr_purchase.batch_id and elf_tr_purchase.deleted=0 and elf_ma_event.deleted=0 and elf_ma_batch.deleted=0 and elf_tr_purchase.product_type='.$type;
    	}
    		// $res= "SELECT * FROM `".$table_name."` where deleted=0";
	     else if($type ==0 && $table_name=='elf_tr_purchase'){			
	     	$res='select elf_tr_purchase.id as id,elf_ma_batch.batch_id as batch_name,elf_ma_course.course_id as product_id,elf_ma_course.course_name as product_name,parent_name,parent_email,parent_mobile,student_class,student_name,if(type=1,"Online","Offline") as type,elf_tr_purchase.created as reg_date,elf_ma_batch.schedule_day as schedule_day,elf_ma_batch.schedule_time as schedule_time from elf_tr_purchase,elf_ma_course,elf_ma_batch where  elf_ma_course.id = elf_ma_batch.reference_id and elf_ma_batch.id = elf_tr_purchase.batch_id and elf_tr_purchase.deleted=0 and elf_ma_course.deleted=0 and elf_ma_batch.deleted=0 and elf_tr_purchase.product_type=1 UNION select elf_tr_purchase.id as id,elf_ma_batch.batch_id as batch_name,elf_ma_event.event_id as product_id,elf_ma_event.event_name as product_name,parent_name,parent_email,parent_mobile,student_class,student_name,if(type=1,"Online","Offline") as type,elf_tr_purchase.created as reg_date,elf_ma_batch.schedule_day as schedule_day,elf_ma_batch.schedule_time as schedule_time from elf_tr_purchase,elf_ma_event,elf_ma_batch where  elf_ma_event.id = elf_ma_batch.reference_id and elf_ma_batch.id = elf_tr_purchase.batch_id and elf_tr_purchase.deleted=0 and elf_ma_event.deleted=0 and elf_ma_batch.deleted=0 and elf_tr_purchase.product_type=2';  
	     	// error_log('sdfsdf');W
	     }
	      if($res !=''){
		  	$result = mysqli_query($this->db,$res);                         	 			  
		  	if(mysqli_num_rows($result) > 0)
			{		
				while($data=mysqli_fetch_assoc($result)){						
					$alldata[$i] = $data;
					$i++;			
				 }						
			}
		}
		  
		  return array('status'=>200,'data'=>$alldata);
    }

}
?>
