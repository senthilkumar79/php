<!DOCTYPE html>
<html lang="en">
<head>
  <title>ELF - Interactive LIVE Classes for  English Learnig</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="ELF_PHONICS_LOGO.ico" type="image/icon" sizes="16x16">
  <!-- font -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Livvic:wght@600&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Fredoka+One&display=swap" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet">

  <!-- css start -->
  <link rel="stylesheet" type="text/css" href="webroot/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="webroot/css/style.css">
  <link rel="stylesheet" type="text/css" href="webroot/css/responsive.css">
  <!-- <link rel="stylesheet" href="webroot/css/owl.carousel.min.css" type="text/css"> -->
  <!-- <link rel="stylesheet" href="webroot/css/owl.theme.min.css" type="text/css"> -->
  <!-- js start -->
  <script type="text/javascript" src="webroot/js/jquery-3.5.1.js"></script>
  <script type="text/javascript" src="webroot/js/bootstrap.min.js"></script>
  <!-- <script type="text/javascript" src="webroot/js/popper.min.js"></script> -->
  <!-- <script type="text/javascript" src="webroot/js/owl.carousel.min.js"></script> -->
    <!-- <script type="text/javascript" src="webroot/js/script.js"></script> -->
</head>

<body>
    
  <!-- header ends here and Parents Chat  Start-->
       <div id="header"><?php include('header.php');?></div>
   
       <div class="container-fluid elf_banner">
        <div class="container text-center">
          <div class="clearfix">&nbsp;</div>
          <h3>Welcome to  <img src="../webroot/img/elf_logo.png" class="img-fluid" alt="elf_logo" width="220px"></h3>
                        <h4>You will be redirecting to our website shortly....</h4>
          
        </div>
      </div>
  </div>

  <div id="footer">
    <?php include('footer.php');?>
  </div>

<script type="text/javascript">
  $('document').ready(function(e){
    var screen_height = window.innerHeight - $('#footer').height();

      $('.elf_banner').css('height',screen_height);
  });
  // setTimeout(function(){ 
  //   window.location.href='http://localhost:8091';
  // }, 500);

</script>

</body>
</html>