<footer class="mainfooter" role="contentinfo" id="footer">
  <div class="footer-middle">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-6 col-sm-3 offset-md-0">
        <!--Column1-->
        <div class="footer-pad">
          <h4>ELF English</h4>
          <ul class="list-unstyled">
            <li><a href="#">About us</a></li>
            <li><a href="#explore_courses">LIVE Online Courses</a></li>
            <li><a href="#">How it Works</a></li>
            <li><a href="#">Blog</a></li>
            <li><a href="#">School Programs</a></li>
            <li><a href="#">Colleges and Companies</a></li>
            <li><a href="">Books and Kits</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-4 offset-md-1 col-6 col-sm-3">
        <!--Column1-->
        <div class="footer-pad">
          <h4>Contact us</h4>
          <p>New No.9, Old No.5, 2nd Floor, Rajakrishna Road, Teynampet, Chennai - 600018, India</p>
          <a href="">+91 96001 86000</a>
          <ul class="list-unstyled">
            <li><a href="https://www.elflearn.com/" target="_blank">elflearn.com</a></li>
            <li><a href="#">info@elflearn.com</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-3 offset-md-1 social-network">
        <h4>Social Media</h4>
            <ul class="social-network social-circle">
             <li><a href="https://www.instagram.com/elfenglishlearn/" target="_blank"  class="" title="Facebook"><img src="/webroot/img/instagram.png" class="instagram"></a></li>
             <li><a href="https://www.facebook.com/ELFEnglishLearn/" target="_blank" class="" title="Linkedin"><img src="/webroot/img/fb.png" class="fb"></a></li>
           <!--    <li><a href="#" class="" title="Linkedin"><img src="webroot/img/youtube.png"></a></li> -->
            </ul> 
               <ul class="list-unstyled list_top_fix">
               <li><a href="Terms_and_Conditions.php" target="_blank">Terms & Conditions</a></li>
            <li><a href="Privacy_Policy.php" target="_blank">Privacy Policy</a></li>
                        <li><a href="Refund_Policy.php" target="_blank">Refund Policy</a></li>
                        <li><a href="Disclaimer.php" target="_blank">Disclaimer</a></li>

          </ul>      
    </div>
    </div>
  <div class="row">
    <div class="col-md-12 copy">
      <p class="text-center">&copy;2021 ELF Learning Solutions Private Limited, All rights reserved.</p>
    </div>
  </div>
  </div>
  </div>
</footer>
   </body>
</html>