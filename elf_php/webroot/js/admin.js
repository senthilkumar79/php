  $(document).ready(function() {
    $('input[name="batch_start_date"]').datepicker({ minDate: 0, dateFormat: 'yy-mm-dd' });


    $( ".start_date" ).click(function(){
              $(this).blur();
          });
        $( ".start_date").keyup(function(){
                       $(this).blur();
        });
} );
var course_ids=new Array(),batch_ids=new Array(),event_ids=new Array(),trial_class_ids=new Array();
var courses=new Array(),batches = new Array(),events = new Array(),trial_classes = new Array();

function getReportData(tname,type){
 var data = {tname:tname,type:type};
 console.log('#'+tname+'_'+type);
  table = $('#'+tname+'_'+type).DataTable({destroy:true});
  table.rows().remove().draw(); 
  $('.ajaxload').show();
     $.ajax({
           type: "POST",
           url: "/admin/getReportData",
           data: data,
           cache: false,                             
           success: function(data){            
            $('.ajaxload').hide();
              if(data.status ==200){                        
                console.log(data);
                if(tname =='elf_tr_trial_class_booking'){
                  $.each(data.data,function(i,d){   
                      table.row.add([(parseInt(i)+1),d.batch_name,d.trial_class,d.course_id,d.course_name,d.type,d.parent_name,d.parent_email,d.parent_mobile,d.student_class,d.student_reading_level,d.student_speaking_level]).draw().node().id = d.id;             
                  });
                }else if(tname =='elf_tr_trial_booking'){
                  $.each(data.data,function(i,d){   
                      table.row.add([(parseInt(i)+1),d.course_id,d.course_name,d.parent_name,d.parent_email,d.parent_mobile,d.student_class]).draw().node().id = d.id;             
                  });
                }else if(tname =='elf_tr_purchase' && type >0){
                  $.each(data.data,function(i,d){   
                      table.row.add([(parseInt(i)+1),d.batch_name,d.product_id,d.product_name,d.type,d.parent_name,d.parent_email,d.parent_mobile,d.student_name,d.student_class]).draw().node().id = d.id;             
                  });                
                }else if(tname =='elf_tr_purchase' && type ==0){
                  $.each(data.data,function(i,d){   
                    console.log(d.id);
                      table.row.add([(parseInt(i)+1),d.batch_name,d.product_id,d.product_name,d.type,d.parent_name,d.parent_email,d.parent_mobile,d.student_name,d.student_class]).draw().node().id = d.id;             
                  });
                }       
            }     
          } 
   });
}

function getData(tname,ele){  
  var data = {tname:tname};
  table = $('#'+tname).DataTable({destroy:true});
  table.rows().remove().draw(); 
  $('.ajaxload').show();
     $.ajax({
           type: "POST",
           url: "/admin/getData",
           data: data,
           cache: false,                             
           success: function(data){            
              if(data.status ==200){                                               
                  // setTimeout(function () {
                      $.each(data.data,function(i,d){
                          // var edit = '<button class="btn btn-primary" title="edit" onclick="editData('+d.id+',\''+tname+'\')"><i class="fa fa-pencil" aria-hidden="true"></i> </button> <button class="btn btn-danger" title="delete" onclick="deleteDate('+d.id+',\''+tname+'\')"><i class="fa fa-trash" aria-hidden="true"></i> </button>';
                          var edit = ' <button class="btn btn-danger" title="delete" onclick="deleteDate('+d.id+',\''+tname+'\')"><i class="fa fa-trash" aria-hidden="true"></i> </button>';
                          if(tname =='elf_ma_course'){    
                              course_ids.push({'id':d.id,'name':d.course_id});                      
                              if(d.discounted_fee !=0 && d.payment_link ==''){
                                d.payment_link = '<button type="button" class="btn btn-info btn-sm" title="generate payment link for full payment" onclick="generatelink('+d.id+',1,2)" style="display: inline-flex;"> <i class="fa fa-link" aria-hidden="true"></i></button>';
                              }else if(d.discounted_fee !=0 && d.payment_link !=''){     
                                d.payment_link ='<input class="form-control link" id="flink_'+d.id+'" value="'+d.payment_link+'" readonly/><br/><button type="button" class="btn btn-info btn-sm" title="copy payment link for full payment" onclick="copylink(this)" style="margin-top: 5px"> <i class="fa fa-copy" aria-hidden="true"></i>&nbsp;<span class="tooltiptext" id="myTooltip">Copy to clipboard</span></button>';
                              }
                              if(d.part_payment !=0 && d.part_payment_link ==''){
                                d.part_payment_link = '<button type="button" class="btn btn-info btn-sm" title="generate payment link for part payment" onclick="generatelink('+d.id+',2,2)" style="display: inline-flex;"> <i class="fa fa-link" aria-hidden="true"></i></button>';
                              }
                              else if(d.part_payment !=0 && d.part_payment_link !=''){
                                d.part_payment_link ='<input class="form-control link" id="plink_'+d.id+'" value="'+d.part_payment_link+'" readonly/><br/><button type="button" class="btn btn-info btn-sm" title="copy payment link for full payment" onclick="copylink(this)" style="margin-top: 5px"> <i class="fa fa-copy" aria-hidden="true"></i>&nbsp;<span class="tooltiptext" id="myTooltip">Copy to clipboard</span></button>';
                              }
                              table.row.add([(parseInt(i)+1),d.course_id,d.course_name,d.class,d.fees,d.part_payment,d.part_payment_link,d.discounted_fee,d.payment_link,edit]).draw().node().id = d.id;
                            }
                          else if(tname =='elf_ma_trial_class'){                            
                            trial_class_ids.push({'id':d.id,'name':d.trial_class_id});
                            if(d.registration_link =='')
                                d.registration_link = '<button type="button" class="btn btn-info btn-sm" title="generate payment link for full payment" onclick="generatelink('+d.id+',0,1)" style="display: inline-flex;"> <i class="fa fa-link" aria-hidden="true"></i></button>';
                            else
                              d.registration_link ='<input class="form-control link" id="rlink_'+d.id+'" value="'+d.registration_link+'" readonly/><br/><button type="button" class="btn btn-info btn-sm" title="copy payment link for full payment" onclick="copylink(this)" style="margin-top: 5px"> <i class="fa fa-copy" aria-hidden="true"></i>&nbsp;<span class="tooltiptext" id="myTooltip">Copy to clipboard</span></button>';
                            table.row.add([(parseInt(i)+1),d.trial_class_id,d.course_id,d.course_name,d.class,d.registration_link,edit]).draw().node().id = d.id;
                          }
                          else if(tname =='elf_ma_event'){ 
                            // event_ids['"'+d.id+'"'] = d.event_id;   
                            event_ids.push({'id':d.id,'name':d.event_id});                                              
                            if(d.payment_link =='')
                                d.payment_link = '<button type="button" class="btn btn-info btn-sm" title="generate payment link for full payment" onclick="generatelink('+d.id+',1,3)" style="display: inline-flex;"> <i class="fa fa-link" aria-hidden="true"></i></button>';
                            else
                              d.payment_link ='<input class="form-control link" id="flink_'+d.id+'" value="'+d.payment_link+'" readonly/><br/><button type="button" class="btn btn-info btn-sm" title="copy payment link for full payment" onclick="copylink(this)" style="margin-top: 5px"> <i class="fa fa-copy" aria-hidden="true"></i>&nbsp;<span class="tooltiptext" id="myTooltip">Copy to clipboard</span></button>';
                            table.row.add([(parseInt(i)+1),d.event_id,d.event_name,d.class,d.fees,d.discounted_fee,d.payment_link,edit]).draw().node().id = d.id;
                          }
                          else if(tname =='elf_ma_batch'){
                            // batch_ids['"'+d.id+'"'] = d.batch_id;
                            batch_ids.push({'id':d.id,'name':d.batch_id});                  
                            table.row.add([(parseInt(i)+1),d.category_name,d.reference_category,d.reference_name,d.batch_id,d.batch_start_date,d.schedule_day+'<br/>'+d.schedule_time,d.max_batch_size,edit]).draw().node().id = d.id;
                          }


                      });
                console.log($(ele).length);
                        if(ele !=null && $(ele).length >0)
                          populate_category(ele);
                      $('.ajaxload').hide();                
                    // },500);
                
              }
        }});   
}

function validate(form){
  if($(form).find('input.invalid').length ==0){
      var formData = $(form).serializeArray();
      $('.ajaxload').show();
         $.ajax({
               type: "POST",
               url: "/admin/saveData",
               data: formData,
               cache: false,                             
               success: function(data){
                console.log(data);
                $('.ajaxload').hide();
                if(data.status ==200){
                  $('#myModal').modal('hide');
                  getData($(form).find('input[name="table"]').val(),null);
                }else{
                                    alert(data.error_msg);
                }
               }});
         return false;
       }
        return false;
}

$(function(){
  $(document).ready(function(){

    $('.add_newbtn').on('click',function(){
        var href=$(this).attr('data-href');
        // console.log(href);
        if(href=='#trial-tab'){
          var options='';
          $.each(course_ids,function(i,e){
            console.log(e);
            options +='<option value='+e.id+'>'+e.name+'</option>';
          });          
          // course_ids.forEach(function(i,obj){
          //   console.log(i);
          // });
          $('#myModal').find('.tab-pane'+href).find('select[name="course_id"]').find('option').not(':first').remove();          
          $('#myModal').find('.tab-pane'+href).find('select[name="course_id"]').append(options);  
        }
        $('#myModal').find('.tab-pane').removeClass('show active');        
        $('#myModal').find('.tab-pane'+href).addClass('show active');
    });

    $('#myModal').on('hide.bs.modal', function () {
        $(this).find('.tab-pane.active').find('form')[0].reset();
        $('.invalid-feedback').html('').hide();
      });
    });

      

})

function checkduplicates(variable,ele){
  var ob_var = Object.values(variable);
    $.each(ob_var,function(i,obj){
        ob_var[i] = obj.name.toLowerCase();
    });
  console.log(ob_var);
  if(ob_var.indexOf($.trim($(ele).val()).toLowerCase()) !==-1)  
  {
         // $(ele).focus();
         $(ele).removeClass('valid');
         $(ele).addClass('invalid');
         $(ele).parents('.form-group').find('.invalid-feedback').html('Duplicate Id').show();
       }
  else{
        $(ele).parents('.form-group').find('.invalid-feedback').html('').hide();
        $(ele).removeClass('invalid');
         $(ele).addClass('valid');
      }
}

function validate_amount(e){
  var form = $(e).parents('form');
  var fee = isNaN(parseInt($(e).parents('form').find('input[name="fees"]').val())) == true ? 0 : parseInt($(e).parents('form').find('input[name="fees"]').val());
  var discounted_fee = isNaN(parseInt($(e).parents('form').find('input[name="discounted_fee"]').val())) == true ? 0 : parseInt($(e).parents('form').find('input[name="discounted_fee"]').val());
  var part_payment = isNaN(parseInt($(e).parents('form').find('input[name="part_payment"]').val())) == true ? 0 : parseInt($(e).parents('form').find('input[name="part_payment"]').val());
  console.log(part_payment);
  console.log((discounted_fee !=0 && discounted_fee >= fee));
  console.log((part_payment !=0 && discounted_fee <= part_payment));
  if((discounted_fee !=0 && discounted_fee >= fee) || (part_payment !=0 && discounted_fee <= part_payment)){
        $(form).find('input[name="discounted_fee"]').removeClass('valid');
         $(form).find('input[name="discounted_fee"]').addClass('invalid');
         $(form).find('input[name="discounted_fee"]').parents('.form-group').find('.invalid-feedback').html('This field should be < fee && > part_payment').show();         
  }else{
    $(form).find('input[name="discounted_fee"]').removeClass('invalid');
         $(form).find('input[name="discounted_fee"]').addClass('valid');
         $(form).find('input[name="discounted_fee"]').parents('.form-group').find('.invalid-feedback').html('').hide();         
         // if(discounted_fee !=0)
         //      validatelink($(form).find('input[name="payment_link"]'));
  }
  if(part_payment !=0 && (part_payment >= fee || part_payment >= discounted_fee)){
        $(form).find('input[name="part_payment"]').removeClass('valid');
         $(form).find('input[name="part_payment"]').addClass('invalid');
         $(form).find('input[name="part_payment"]').parents('.form-group').find('.invalid-feedback').html('This field should be < fee && < part_payment').show();
         // validatelink($(form).find('input[name="part_payment_link"]'));
      // if($.trim($(form).find('input[name="part_payment_link"]').val()) ==''){
      //      $(form).find('input[name="part_payment_link"]').removeClass('valid');
      //      $(form).find('input[name="part_payment_link"]').addClass('invalid');
      //      $(form).find('input[name="part_payment_link"]').parents('.form-group').find('.invalid-feedback').html('Generate link for the above field').show();         
      //    }
  }else{
        $(form).find('input[name="part_payment"]').removeClass('invalid');
         $(form).find('input[name="part_payment"]').addClass('valid');
         $(form).find('input[name="part_payment"]').parents('.form-group').find('.invalid-feedback').html('').hide();         
         // if(part_payment !=0 )
         //  validatelink($(form).find('input[name="part_payment_link"]'));
         
  }
}

function validatelink(e){
        if($.trim($(e).val()) ==''){
           $(e).removeClass('valid');
           $(e).addClass('invalid');
           $(e).parents('.form-group').find('.invalid-feedback').html('Generate link for the above field').show();
         }else{
          $(e).removeClass('invalid');
           $(e).addClass('valid');
           $(e).parents('.form-group').find('.invalid-feedback').html('').hide();
         }
}

function generatelink(id,type,category){
  var data = {id:id,type:type,category:category,generate:1};
   $('.ajaxload').show();
         $.ajax({
               type: "POST",
               url: "/admin/saveData",
               data: data,
               cache: false,                             
               success: function(data){
                console.log(data);
                $('.ajaxload').hide();
                if(data.status ==200){
                  // $('#myModal').modal('hide');
                    $('.nav-tabs').find('a.nav-link.active').click();
                }else{
                  alert(data.error_msg);
                }
               }});
         return false;
}
        

function copylink(e){
console.log($(e).parents('td').find('input.link'));
  if($(e).parents('td').find('input.link').val() !='' ){
      if(copyToClipboard($(e).parents('td').find('input.link'))) {
                $(e).parents('td').find('button > span#myTooltip').html('Copied to Clipboard');
            } else {
                $(e).parents('td').find('button > span#myTooltip').html('Copy Failed');
            }
  }else{
    // validatelink($(e).parents('.form-group').find('input'));
  }
}

function copyToClipboard(elem) {
        // set focus to hidden element and select the content
        console.log($(elem));
        $(elem).focus();
        // select all the text therein  
        $(elem).select();

        var succeed;
        try {
            succeed = document.execCommand("copy");
        } catch(e) {
            succeed = false;
        }

        return succeed;
    }     

function populate_category(ele){
  var category = $(ele).val();
  var options='';
  $('.ajaxload').show();
  if(category ==2){
    if(course_ids.length >0){
        options +='<option value="">Select Course</option>';
        $.each(course_ids,function(i,e){            
                options +='<option value='+e.id+'>'+e.name+'</option>';
              });          
      }else{
        getData('elf_ma_course',ele);
        // populate_category(ele);
      }
  }else if(category ==1){
    if(trial_class_ids.length >0){
      options +='<option value="">Select Trial Class</option>';
        $.each(trial_class_ids,function(i,e){            
                options +='<option value='+e.id+'>'+e.name+'</option>';
              });  
      }else{
        getData('elf_ma_trial_class',ele);
        // populate_category(ele);
      }        
  }
  else if(category ==3){
    if(event_ids.length >0){
      options +='<option value="">Select Event</option>';
        $.each(event_ids,function(i,e){            
                options +='<option value='+e.id+'>'+e.name+'</option>';
              });          
        }else{
        getData('elf_ma_event',ele);
        // populate_category(ele);
      }  
  }
  // $(ele).parents('form').find('select[name="reference_id"]').find('option').not(':first').remove();          
  $(ele).parents('form').find('select[name="reference_id"]').html(options);
  $('.ajaxload').hide();

}

function editData(id,tname){

}

function deleteDate(id,tname){
  if(confirm('Are you sure? You want to delete the record?? All trail / batch associated will be lost')){
      var data = {id:id,tname:tname,deleted:1};
      $('.ajaxload').show();
             $.ajax({
                   type: "POST",
                   url: "/admin/saveData",
                   data: data,
                   cache: false,                             
                   success: function(data){
                    console.log(data);
                    $('.ajaxload').hide();
                    if(data.status ==200){
                        getData(tname,null);
                      // $('#myModal').modal('hide');
                        // $('.nav-tabs').find('a.nav-link.active').click();
                    }else{
                      alert(data.error_msg);
                    }
                   }});
             return false;
  }

}