drop database if exists elf;
CREATE DATABASE elf;
use elf;
drop table if exists elf_tr_purchase;
drop table if exists elf_tr_trial_booking;
drop table if exists elf_tr_trial_class_booking;
drop table if exists elf_ma_event;
drop table if exists elf_ma_course;
drop table if exists elf_ma_trial_class;
drop table if exists elf_ma_batch;

Create table elf_ma_course(
	id int(11) NOT NULL AUTO_INCREMENT,
	course_id varchar (30) not null,
	course_name varchar (100) not null,
	class varchar (20) not null,
	fees int not null default 0,
	discounted_fee int not null default 0,
	payment_link varchar(200) not null,
	part_payment int(11) not null default 0,
	part_payment_link varchar(200) not null,
	created TIMESTAMP DEFAULT NOW(),
  	modified TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	deleted int not null default 0,
	PRIMARY KEY (`id`),
	UNIQUE KEY `course_id` (`course_id`),
	KEY `elf_ma_course_index` (`id`,`course_id`,`class`,`deleted`,`created`,`modified`,`fees`,`discounted_fee`)
)ENGINE=InnoDB;


Create table elf_ma_trial_class(
	id int(11) NOT NULL AUTO_INCREMENT,
	course_id int(11) not null,
	trial_class_id varchar(30),
	registration_link varchar(200) not null,
	created TIMESTAMP DEFAULT NOW(),
	modified TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	deleted int not null default 0,
	PRIMARY KEY (`id`),
	UNIQUE KEY `trial_class_id` (`trial_class_id`),
	KEY `elf_fk_course_id` (`course_id`),  
	CONSTRAINT `elf_fk_course_id` FOREIGN KEY (`course_id`) REFERENCES `elf_ma_course` (`id`),
	KEY `elf_ma_trial_class_index` (`id`,`course_id`,`trial_class_id`,`deleted`,`created`,`modified`)
)ENGINE=InnoDB;



Create table elf_ma_event(
	id int(11) NOT NULL AUTO_INCREMENT,
	event_id varchar(30) not null,
	event_name varchar (100) not null,
	payment_link varchar(200) not null,
	fees int not null default 0,
	class varchar (20) not null,
	discounted_fee int not null default 0,
	deleted int not null default 0,
	created timestamp DEFAULT NOW(),
	modified timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`),
	UNIQUE KEY `event_id` (`event_id`),
	KEY `elf_ma_event_index` (`id`,`event_id`,`fees`,`class`,`discounted_fee`,`deleted`,`created`,`modified`)
)ENGINE=InnoDB;




Create table elf_ma_batch(
	id int(11) NOT NULL AUTO_INCREMENT,	
	reference_id int(11) not null,
	category int(11) not null default 0 comment '1 - trial_class_id, 2 - course_id,3 - event_id',
	batch_id varchar(30) not null,
	batch_start_date date not null default '0000-00-00',
	schedule_day varchar(75),
	schedule_time varchar(75),
	max_batch_size int not null default 0,
	deleted int not null default 0,
	created timestamp DEFAULT NOW(),
	modified timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`),	
	UNIQUE KEY `batch_id` (`batch_id`),
	KEY `elf_ma_batch_index` (`id`,`reference_id`,`category`,`batch_id`,`max_batch_size`,`deleted`,`created`,`modified`)
)ENGINE=InnoDB;



create table elf_tr_trial_class_booking( -- trial booking inside course
	id int(11) NOT NULL AUTO_INCREMENT,
	parent_name varchar(75) not null,
	parent_mobile varchar(20) not null,
	parent_email varchar(50) not null,
	trial_class_id int(11) not null, -- foregin key
	batch_id int(11) not null,
	student_class varchar(10) not null,
	student_reading_level varchar(50),
	student_speaking_level varchar(50),
	type int(11) not null default 0 comment '1-online , 2-offline',
	deleted int not null default 0,
	created timestamp DEFAULT NOW(),
	modified timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`),
	KEY `elf_fk_trial_id` (`trial_class_id`),  
	CONSTRAINT `elf_fk_trial_id` FOREIGN KEY (`trial_class_id`) REFERENCES `elf_ma_trial_class` (`id`),	
	KEY `elf_fk_batch_id` (`batch_id`),  
	CONSTRAINT `elf_fk_batch_id` FOREIGN KEY (`batch_id`) REFERENCES `elf_ma_batch` (`id`),	
	KEY `elf_tr_trial_class_booking_index` (`id`,`parent_email`,`parent_mobile`,`batch_id`,`trial_class_id`,`type`,`deleted`,`created`,`modified`)
)ENGINE=InnoDB;

create table elf_tr_trial_booking(
	id int(11) NOT NULL AUTO_INCREMENT,
	parent_name varchar(75) not null,
	parent_mobile varchar(20) not null,
	parent_email varchar(75) not null,
	student_class varchar(10) not null,
	student_prefered_course int(11) not null,
	deleted int not null default 0,
	created timestamp DEFAULT NOW(),
	modified timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`),
	KEY `elf_trial_fk_course_id` (`student_prefered_course`),  
	CONSTRAINT `elf_trial_fk_course_id` FOREIGN KEY (`student_prefered_course`) REFERENCES `elf_ma_course` (`id`),
	KEY `elf_tr_trial_booking_index` (`id`,`parent_email`,`parent_mobile`,`student_class`,`student_prefered_course`,`deleted`,`created`,`modified`)
)ENGINE=InnoDB;

drop table if exists elf_tr_purchase;
create table elf_tr_purchase(
	id int(11) NOT NULL AUTO_INCREMENT,
		parent_name varchar(75) not null,
		parent_mobile varchar(20) not null,
		parent_email varchar(50) not null,
		student_class varchar(10) not null,
		student_name varchar(75) not null,
		product_id int(11) not null,
		batch_id int(11) not null,
		product_type int(11) not null comment '1-course , 2-event',
		order_id varchar(45) not null,
		tracking_id varchar(45) not null,
		order_status varchar(15) not null,
		type int(11) not null default 0 comment '1-online , 2-offline',
		amount int(11) not null default 0,
		deleted int not null default 0,
		created timestamp DEFAULT NOW(),
		modified timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		PRIMARY KEY (`id`),
		KEY `elf_tr_purchase_index` (`id`,`parent_email`,`parent_mobile`,`product_type`,`order_status`,`order_id`,`tracking_id`,`type`,`amount`,`deleted`,`created`,`modified`,`product_id`,`batch_id`)
)ENGINE=InnoDB;


create table elf_ma_admin(
	id int(11) NOT NULL AUTO_INCREMENT,
	name varchar(75) not null,
	email varchar(50) not null,
	password varchar(10) not null,
	role int(11) not null comment '1-admin,2-coordinator',
	deleted int not null default 0,
	created timestamp DEFAULT NOW(),
	modified timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`),
	KEY `elf_ma_admin_index` (`id`,`email`,`role`,`deleted`,`created`,`modified`)
)ENGINE=InnoDB;

