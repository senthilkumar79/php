<?php
if(!isset($_SESSION))
  session_start();
include_once('../dbconnect.php');
if(isset($_SESSION) && isset($_SESSION['userData']['id']))
{

  $con= new db();
  if(isset($_POST['table'])){
      $data = $con->saveData($_POST);
  	  if(!empty($data)){
  	  	 header("Content-Type:application/json");      	  
			  echo json_encode($data);			      
		}
	}else if(isset($_POST['generate'])){
    $updateData =array();
    //type = 0 - registration link
    //type =1 - payment link
    //type =2 - part payment link
    $table='';
    if($_POST['category'] ==2)
        $table= 'elf_ma_course';
    else if($_POST['category'] ==1)
        $table= 'elf_ma_trial_class';
    else if($_POST['category'] ==3)
        $table= 'elf_ma_event';
      /*generate link logic*/
        if($_POST['category'] ==2 || $_POST['category'] == 3){ //course || event
          $encoded = urlencode(base64_encode($_POST['id'].'##'.$_POST['category'].'##'.$_POST['type'].'@'.round(microtime(true) * 1000)));
          $_POST['payment_link']='';$_POST['part_payment_link']='';
          if($_POST['type'] ==2)
              $_POST['part_payment_link']  = 'http://'.$_SERVER['HTTP_HOST'].'/payu/'.$encoded;        
          if($_POST['type'] ==1)
              $_POST['payment_link']  = 'http://'.$_SERVER['HTTP_HOST'].'/payu/'.$encoded;      

          $updateData = array('part_payment_link'=>$_POST['part_payment_link'],'payment_link'=>$_POST['payment_link']);                  
        }
        else if($_POST['category'] ==1){ //trial
              $encoded = urlencode(base64_encode($_POST['id'].'##'.$_POST['category'].'##'.$_POST['type'].'@'.round(microtime(true) * 1000)));            
                $_POST['registration_link']  = 'http://'.$_SERVER['HTTP_HOST'].'/trial-register/'.$encoded;    
                $updateData = array('registration_link'=>$_POST['registration_link']);                
          }
      /*generate link logic ends*/
      if(!empty($updateData)){
        // error_log(print_r($updateData,true));
        $data = $con->updateData($table,$updateData,array('id'=>$_POST['id']));
        if(!empty($data)){
           header("Content-Type:application/json");         
          echo json_encode($data);            
        }
      }else{
        echo json_encode(array('status'=>500,'error_msg'=>'OOPs!! Something went wrong'));
      }
  }else if(isset($_POST['deleted'])){
        $data = $con->updateData($_POST['tname'],array('deleted'=>$_POST['deleted']),array('id'=>$_POST['id']));
        if(!empty($data)){
           header("Content-Type:application/json");         
          echo json_encode($data);            
        }
  }
}else{
	header("Location: login");
}

?>
