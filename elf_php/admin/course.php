<?php
if(!isset($_SESSION))
  session_start();
include_once('../dbconnect.php');
include_once('../constant.php');
if(isset($_SESSION) && isset($_SESSION['userData']['id']))
{

  $con= new db();  
?>

  <!DOCTYPE html>
  <html lang="en">
  <head>
    <title>ELF - Interactive LIVE Classes for  English Learnig</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="ELF_PHONICS_LOGO.ico" type="image/icon" sizes="16x16">
    <!-- font -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Livvic:wght@600&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Fredoka+One&display=swap" rel="stylesheet">

    <!-- css start -->
    <link rel="stylesheet" type="text/css" href="/webroot/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/webroot/css/admin.css">
    <link rel="stylesheet" type="text/css" href="/webroot/css/dataTables.bootstrap4.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet">
    <!-- <link rel="stylesheet" type="text/css" href="/webroot/css/bootstrap-datepicker.css"> -->
    <!-- js start -->
     <script type="text/javascript" src="/webroot/js/jquery-3.5.1.js"></script>
     <script type="text/javascript" src="/webroot/js/bootstrap.min.js"></script>
     <link rel="stylesheet" href="/webroot/css/jquery-ui.css">
<!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
<script src="/webroot/js/jquery-ui.js"></script>
     <script type="text/javascript" src="/webroot/js/jquery.dataTables.min.js"></script>
     <script type="text/javascript" src="/webroot/js/dataTables.bootstrap4.min.js"></script>
     <!-- <script type="text/javascript" src="/webroot/js/bootstrap-datepicker.js"></script> -->
     
     
<script type="text/javascript" src="/webroot/js/admin.js"></script>  
  </head>
  <body>

  	<!-- header ends here and Parents Chat  Start-->
  <nav class="header navbar navbar-expand-md navbar-light">
        <div class="container">

          <a href="index.html" class="navbar-brand ag_logo"><img src="/webroot/img/elf_logo.png" class="img-fluid" alt="elf_logo"></a>


          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

          <div class="collapse navbar-collapse" id="navbarNav">
            <div class="navbar-nav  ml-auto">              
              <a href="reports" class="nav-item nav-link">Reports</a>
              <a href="logout" class="nav-item nav-link">Logout</a>
              </div>
          </div>
        </div>
      </nav>

  <section id="tabs" class="project-tab">
              <div class="container">
                <button class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i> Add User</button>
                <div class="clearfix">&nbsp;</div>
                  <div class="row">
                      <div class="col-md-12 main_tab">
                          <nav>
                              <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                  <a class="nav-item nav-link active" id="nav-course-tab" data-toggle="tab" href="#nav-course" role="tab" aria-controls="nav-home" aria-selected="true"  onclick="getData('elf_ma_course');">Course</a>
                                  <a class="nav-item nav-link" id="nav-trial-tab" data-toggle="tab" href="#nav-trial" role="tab" aria-controls="nav-profile" aria-selected="false" onclick="getData('elf_ma_trial_class');">Trial Class</a>
                                  <a class="nav-item nav-link" id="nav-event-tab" data-toggle="tab" href="#nav-event" role="tab" aria-controls="nav-batch" aria-selected="false"  onclick="getData('elf_ma_event');">Event</a>
                                  <a class="nav-item nav-link" id="nav-batch-tab" data-toggle="tab" href="#nav-batch" role="tab" aria-controls="nav-event" aria-selected="false"  onclick="getData('elf_ma_batch');">Batch</a>
                                  <!-- <a class="nav-item nav-link" id="nav-adduser-tab" data-toggle="tab" href="#nav-adduser" role="tab" aria-controls="nav-adduser" aria-selected="false">Add User</a> -->
                              </div>
                          </nav>
                          <div class="tab-content" id="nav-tabContent">
                                <div class="ajaxload"></div>
                              <div class="tab-pane fade show active" id="nav-course" role="tabpanel" aria-labelledby="nav-home-tab">            
                                  <div class="container">
                                    <div class="">
                                      <button type="button" class="btn btn-info add_newbtn showSingle" target="1" data-href='#course-tab' data-toggle="modal" data-target="#myModal" title="add course"><i class="fa fa-plus" aria-hidden="true"></i> Add New Course</button>
                                    </div>
                                <div class="text-center">
                                  <h2>Course List</h2>
                                </div>
                                 <div class="col-md-12 table-responsive-lg">
                                   <table id="elf_ma_course" class="table table-striped table-bordered table-responsive parents_table" style="width:100%">
                               <thead>
                                    <tr>
                                        <th>Srno</th>
                                        <th>Course ID</th>
                                        <th>Course Name</th>
                                        <th>Class</th>
                                        <th>Fees</th>
                                        <th>Part Payment(3 months)</th>
                                        <th>Part Payment Link</th>
                                        <th>Discounted Full Payment</th>
                                        <th>Full Payment Link</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  
                                  </tbody>
                                  </table>
                              </div>
                          </div>
                        </div>
                        <div class="tab-pane fade" id="nav-trial" role="tabpanel" aria-labelledby="nav-profile-tab">
                            <div class="container">
                              <div class="">
                                <button type="button" class="btn btn-info add_newbtn showSingle" target="2" data-href='#trial-tab' data-toggle="modal" data-target="#myModal" title="add trial class"><i class="fa fa-plus" aria-hidden="true"></i> Add New Trial Class</button>
                               </div>
                             <div class="text-center">
                                  <h2>Trial Class List</h2>
                            </div>
                             <div class="col-md-12 table-responsive-lg">
                               <table id="elf_ma_trial_class" class="table table-striped table-bordered table-responsive parents_table col-md-12">
                           <thead>
                                <tr>
                                    <th>Srno</th>
                                    <th>Trial Class ID</th>                            
                                    <th>Course ID</th>
                                    <th>Course Name</th>
                                    <th>Class</th>
                                    <th id='reglink'>Registration Link</th>
                                    <th>Action</th>
                                  </tr>
                            </thead>
                            <tbody>                         
                              </tbody>
                              </table>
                          </div>
                        </div>
                        </div>
                          <div class="tab-pane fade" id="nav-event" role="tabpanel" aria-labelledby="nav-batch">
                              <div class="container">
                                    <div class="">
                                      <button type="button" class="btn btn-info add_newbtn showSingle" target="3" data-href='#event-tab' data-toggle="modal" data-target="#myModal" title="add event"><i class="fa fa-plus" aria-hidden="true"></i> Add New Event</button>
                                    </div>
                                <div class="text-center">
                                  <h2>Event List</h2>
                                </div>
                                 <div class="col-md-12 table-responsive-lg">
                                   <table id="elf_ma_event" class="table table-striped table-bordered table-responsive  parents_table" style="width:100%">
                                   <thead>
                                          <tr>
                                              <th>Srno</th>
                                              <th>Event ID</th>
                                              <th>Event Name</th>
                                              <th>Class</th>
                                              <th>Fees</th>                                                            
                                              <th>Discounted Fees</th>
                                              <th id='reglink'>Purchase Link</th>
                                              <th>Action</th>
                                          </tr>
                                      </thead>
                                      <tbody>                          
                                        </tbody>
                                        </table>
                                  </div>
                              </div>
                        </div>

                          <div class="tab-pane fade" id="nav-batch" role="tabpanel" aria-labelledby="nav-event">
                              <div class="container">
                                    <div class="">
                                      <button type="button" class="btn btn-info add_newbtn showSingle" target="4" data-href='#batch-tab' data-toggle="modal" data-target="#myModal" title="add batch"><i class="fa fa-plus" aria-hidden="true" ></i> Add New Batch</button>
                                    </div>
                                <div class="text-center">
                                  <h2>Batch List</h2>
                                </div>
                                 <div class="col-md-12 table-responsive-lg">
                                   <table id="elf_ma_batch" class="table table-striped table-bordered table-responsive  parents_table" style="width:100%">
                                     <thead>
                                          <tr>
                                              <th>Srno</th>
                                              <th>Category</th>
                                              <th>Reference Id</th>
                                              <th>Reference Name</th>
                                              <th>Batch ID</th>
                                              <th>Batch Start Date</th>
                                              <th>Schedule Day & time</th>                              
                                              <th>Max Batch Size</th>
                                              <th>Action</th>
                                          </tr>
                                      </thead>
                                        <tbody>                     
                                        </tbody>
                                        </table>
                                  </div>
                              </div>
                          </div>
                            <!--  <div class="tab-pane fade" id="nav-adduser" role="tabpanel" aria-labelledby="nav-adduser">
                              <div class="container">
                                    <div class="">
                                      <button type="button" class="btn btn-info add_newbtn showSingle" target="5" class="btn btn-info add_newbtn" data-toggle="modal" data-target="#myModal">Add New User</button>
                                    </div>
                                <div class="text-center">
                                  <h2>Add User</h2>
                                </div>
                       <div class="col-md-12 table-responsive-lg">
                         <table id="example" class="table table-striped table-bordered table-responsive  parents_table" style="width:100%">
                     <thead>
                          <tr>
                              <th>Category</th>
                              <th>Course /<br> Trial Class /<br> Event ID</th>
                              <th>Batch<br> ID</th>
                              <th>Parent<br> Name</th>
                              <th>Email <br>ID</th>
                              <th>Phone <br>Number</th>
                              <th>Child's <br>Name</th>
                              <th>Child's <br>Class</th>
                              <th>Child's <br>Reading <br>Level</th>
                              <th>Child's <br>Speaking <br>Level</th>
                              <th>Status</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                          </tr>
                        </tbody>
                        </table>
                    </div>
                  </div>
                              </div>-->
                          </div>
                      </div>
                  </div>
              </div>
          </section>

  <!-- footer start -->

  <div class="modal" id='myModal' data-backdrop='static'>
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal body -->
      <div class="modal-body">
              <!-- Tab panes -->
              <div class="ajaxload"></div> 
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <div class="tab-content">
                <div id="course-tab" class="container tab-pane fade show active"><br>
                 <div class="text-center"><h2>Add New Course</h2></div>
                    <div class="main_elf">
                      <form id="bs-validate-demo"  action="" method="post" onsubmit="return validate(this);">
                          <input type="hidden" name="table" value="elf_ma_course" />
                          <div class="row">
                            <div class="form-group col-md-6 col-sm-10">
                              <label for="NameDemo2">Course ID<span class="mandatory">*</span></label>
                              <input  name="course_id" placeholder="" class="form-control col-md-12" maxlength="20" minlength="4" required="" type="text" onkeyup="checkduplicates(course_ids,this);">
                              <div class="invalid-feedback"></div>    
                            </div>  
                            <div class="col-md-6 form-group col-sm-10">
                              <label for="NameDemo1">Course Name<span class="mandatory">*</span></label>
                              <input type="text" name="course_name" class="form-control col-md-12" minlength="4" maxlength="50" required >
                              <div class="invalid-feedback"></div>
                            </div>
                          </div>
                          <div class="row">
                               <div class="col-md-6 form-group col-sm-10">
                                  <label for="selectDemo">Your Class<span class="mandatory">*</span></label>
                                   <select class="form-control col-md-12"  name="class" required="">
                                      <option value="">Select one</option>
                                       <?php foreach ($course_class as $key => $value) {?>
                                              <option value="<?php echo $value;?>"><?php echo $value;?></option>
                                       <?php }?>
                            
                                         </select>
                                           <div class="invalid-feedback"></div>
                                </div>
                                <div class="form-group col-md-6 col-sm-10">
                                      <label for="NameDemo2">Fees<span class="mandatory">*</span></label>
                                         <input  placeholder="" name="fees" class="form-control col-md-12" required="" type="number" onkeyup="validate_amount(this)">
                                            <div class="invalid-feedback">

                                            </div>    
                                        </div>
                                  </div>
                                    
                                        
                                        <div class="row">
                                          <div class="form-group col-md-6 col-sm-10">
                                            <label for="NameDemo2">Discounted Fee</label>
                                           <input  name="discounted_fee"
                                           placeholder="" class="form-control col-md-12" type="number" onkeyup="validate_amount(this)" min=0/>  
                                           <div class="invalid-feedback"></div>  
                                          </div>
                                              <div class="form-group col-md-6  col-sm-10">
                                            <label for="NameDemo2">Part Payment Fees</label>
                                           <input  name="part_payment"
                                           placeholder="" class="form-control col-md-12" type="number" onkeyup="validate_amount(this)" min=0 /> 
                                           <div class="invalid-feedback"></div>   

                                         
                                         </div> 
                                       </div>
                                       <!-- <div class="form-group col-md-12  col-sm-12">
                                            <label for="NameDemo2">Discounted Full Payment Link</label>
                                           <input  name="payment_link"
                                           placeholder="" class="form-control col-md-10 col-sm-10" type="text" readonly />
                                           <div class="invalid-feedback"></div>   
                                             <button type='button' class="btn btn-info btn-sm" title="copy payment link for full payment" onclick="copylink(this)" style='margin-top: 5px'> <i class="fa fa-copy" aria-hidden="true"></i>&nbsp;<span class="tooltiptext" id="myTooltip">Copy to clipboard</span></button>                                           
                                           </div>-->
                                      <!-- <div class="row">
                                        <div class="form-group col-md-8  col-sm-10">
                                        <label for="NameDemo2">Discounted Part Payment Fees(3 months)</label>
                                       <input  name="part_payment"
                                       placeholder="" class="form-control col-md-9 col-sm-9" type="number" onkeyup="validate_amount(this)" min=0 style="display: inline-flex;" />  <button type='button' class="btn btn-info btn-sm" title="generate payment link for part payment" onclick="generatelink(this,1,2)" style="display: inline-flex;"> <i class="fa fa-link" aria-hidden="true"></i></button>
                                       <div class="invalid-feedback"></div>   
                                      </div>                                     -->
                                     
                                     <!--  <div class="form-group col-md-12  col-sm-12">
                                      <label for="NameDemo2">Discounted Part Payment Link</label>
                                     <input  name="part_payment_link"
                                     placeholder="" class="form-control col-md-10 col-sm-10" type="text" readonly />  
                                     <div class="invalid-feedback"></div>   
                                     <button type='button' class="btn btn-info btn-sm" title="copy payment link for part payment" style='margin-top: 5px' onclick="copylink(this)"> <i class="fa fa-copy" aria-hidden="true"></i>&nbsp;<span class="tooltiptext" id="myTooltip">Copy to clipboard</span></button>
                                     
                                    </div>   -->       
                                                    <!-- </div>             -->

                                    <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-info book_btn">Submit</button>
                                      <button data-dismiss='modal' class="btn btn-info book_contiunue">Cancel</button>
                                  </div>
                                  
                                </form>
                            </div>
                </div>
                <div id="trial-tab" class="container tab-pane fade"><br>
                   <div class="text-center">
                        <h2>Add New Trial Class</h2>
                      </div>
                      <div class="main_elf">
                        <form id="bs-validate-demo1"   action="" method="post" onsubmit="return validate(this);">
                                    <input type="hidden" name="table" value="elf_ma_trial_class" />
                           <div class="row"> 
                         <div class="col-md-6 form-group">
                            <label for="selectDemo">Select Course ID<span class="mandatory">*</span></label>
                            <select class="form-control col-md-12" name='course_id' required=""  maxlength="50" minlength="4">
                              <option value="">Select Course</option>
                              <!-- <option value="cours1">PR1 - Learn to Read Phonics</option>
                              <option value="course2">PR2 - Speak Early</option>
                               <option value="cours3">PR3 - Star Reader</option>
                              <option value="course4">PR4 - Speak Up</option>
                              <option value="course5">PR5 - Fast Fluency</option> -->
                            </select>
                             <div class="invalid-feedback">
                                 Please select your Course ID.
                              </div>
                          </div>
                          <div class="form-group col-md-6">
                            <label for="NameDemo2">Trial Class ID<span class="mandatory">*</span></label>
                           <input  placeholder="" class="form-control col-md-12" maxlength="20" name='trial_class_id' minlength="4" required="" type="text" onkeyup="checkduplicates(trial_class_ids,this);">
                              <div class="invalid-feedback">
                                 Please enter a Trial Class ID!
                              </div>    
                          </div> 
                          <div class="col-md-12 text-center">
                          <button type="submit" class="btn btn-info book_btn">Submit</button>
                            <button data-dismiss='modal' class="btn btn-info book_contiunue">Cancel</button>
                        </div>
                        </div>
                        </form>
                  </div>
                </div>
                <div id="event-tab" class="container tab-pane fade"><br>
                 <div class="text-center">
                      <h2>Add New Event</h2>
                    </div>
                    <div class="main_elf">
                      <form id="bs-validate-demo2 "  action="" method="post" onsubmit="return validate(this);">
                                    <input type="hidden" name="table" value="elf_ma_event" />
                         <div class="row">
                        <div class="form-group col-md-6">
                          <label for="NameDemo2">Event ID<span class="mandatory">*</span></label>
                         <input  name="event_id" placeholder="" class="form-control col-md-12" maxlength="20" minlength="3" required="" type="text" onkeyup="checkduplicates(event_ids,this);">
                            <div class="invalid-feedback">
                               Please enter Event ID!
                            </div>    
                        </div>  
                        <div class="col-md-6 form-group">
                          <label for="NameDemo1">Event Name<span class="mandatory">*</span></label>
                          <input type="text"  name="event_name" class="form-control col-md-12" maxlength="50" required>
                              <div class="invalid-feedback">
                                Please enter Event Name.
                              </div>
                        </div>
                       <div class="col-md-4 form-group">
                          <label for="selectDemo">Your Class<span class="mandatory">*</span></label>
                          <select class="form-control col-md-12" name="class" required="">
                            <option value="">Select one</option>
                            <?php foreach ($course_class as $key => $value) {?>
                                              <option value="<?php echo $value;?>"><?php echo $value;?></option>
                                        <?php }?>
                          </select>
                           <div class="invalid-feedback">
                               Please select your class.
                            </div>
                        </div>
                          <div class="form-group col-md-4">
                          <label for="NameDemo2">Fees<span class="mandatory">*</span></label>
                         <input id="get_fees" name="fees" 
                         placeholder="" class="form-control col-md-12" min=0 onkeyup="validate_amount(this)" required="" type="number">
                            <div class="invalid-feedback"></div>
                        </div>
                          <div class="form-group col-md-4">
                          <label for="NameDemo2">Discounted Fees</label>
                         <input id="discounted_fees" name="discounted_fee"
                         placeholder="" class="form-control col-md-12" min=0  onkeyup="validate_amount(this)" type="number">    
                         <div class="invalid-feedback"></div>
                        </div>

                        <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-info book_btn">Submit</button>
                          <button data-dismiss='modal' class="btn btn-info book_contiunue">Cancel</button>
                      </div>
                      </div>
                      </form>
                </div>
                </div>
                <div id="batch-tab" class="container tab-pane fade"><br>
                  <div class="text-center">
                    <h2>Add New Batch</h2>
                  </div>
                  <div class="main_elf">
                    <form id="bs-validate-demo3"   action="" method="post" onsubmit="return validate(this);">
                                    <input type="hidden" name="table" value="elf_ma_batch" />
                       <div class="row">
                     <div class="col-md-6 form-group">
                        <label for="selectDemo">Category<span class="mandatory">*</span></label>
                        <select class="form-control col-md-12" required="" name="category" id="slct1" onchange="populate_category(this)" >
                          <option value="">Select Category</option>
                          <option value=2>Course</option>
                          <option value=1>Trial Class</option>
                          <option value=3>Event</option>
                        </select>
                         <div class="invalid-feedback">
                             Please select your Category.
                          </div>
                      </div>
                      <div class="col-md-6 form-group">
                        <label for="selectDemo">Select Course/Trial Class/Event<span class="mandatory">*</span></label>
                        <select class="form-control col-md-12" name='reference_id' required="" id="slct2" name="slct2">
                                              
                        </select>
                         <div class="invalid-feedback">
                             Please select your Course/Trial Class/Event.
                          </div>
                      </div>
                     <div class="form-group col-md-6">
                        <label for="NameDemo2">Batch ID<span class="mandatory">*</span></label>
                       <input  name="batch_id" placeholder="" class="form-control col-md-12" required="" type="text" onkeyup="checkduplicates(batch_ids,this);">
                          <div class="invalid-feedback">
                             Please enter a Batch ID!
                          </div>    
                      </div>  
                      <div class="form-group col-md-6">
                        <label for="inputDate">Batch Start Date<span class="mandatory">*</span></label>
                            <input type="text" class="form-control start_date" autocomplete="off" data-readonly name="batch_start_date" placeholder="YYYY-MM-DD" required="required" />                            
                          <div class="invalid-feedback">
                             Please select Batch Start Date!
                          </div> 
                      </div>
                      <div class="form-group col-md-6">
                        <label for="NameDemo2">Schedule Days</label>
                       <input  placeholder="" class="form-control col-md-12" name="schedule_day"  type="text" maxlength="60" minlength="6">
                 
                      </div>
                        <div class="form-group col-md-6">
                        <label for="NameDemo2">Schedule Time</label>
                       <input placeholder="" class="form-control col-md-12" name="schedule_time" type="text" maxlength="60" minlength="6">   
                      </div>
                        <div class="form-group col-md-6">
                        <label for="NameDemo2">Max Batch Size</label>
                       <input name="max_batch_size"  placeholder="" class="form-control col-md-12" maxlength="2" minlength="1" type="number">    
                      </div>

                      <div class="col-md-12 text-center">
                      <button type="submit" class="btn btn-info book_btn">Submit</button>
                        <button data-dismiss='modal' class="btn btn-info book_contiunue">Cancel</button>
                    </div>
                    </div>
                    </form>
              </div>

                </div>

              </div>
            </div>
              </div>
            </div>           
    
  </div>


 <!-- <div class="container">
    
    <div class="modal" id="myModal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="col-md-12 main_tab">              
                       <div class="modal-body">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                         <div class="tab-content">
                              <div class="ajaxload"></div> 
                        <div class="container">
                          <div class="tab-pane active" id="elf_ma_course" role="tabpanel">
                              <div class="col-md-12 targetDiv">
                                <div class="text-center">
                                  <h2>Add New Course</h2>
                                </div>
                                <div class="main_elf">
                                  <form id="bs-validate-demo" >
                                     <div class="row">
                                    <div class="form-group col-md-6">
                                      <label for="NameDemo2">Course ID<span class="mandatory">*</span></label>
                                     <input  name="mobile_number" 
                                     placeholder="" class="form-control col-md-12" maxlength="20" minlength="4" required="" type="text">
                                        <div class="invalid-feedback">
                                           Please enter a Course ID!
                                        </div>    
                                    </div>  
                                    <div class="col-md-6 form-group">
                                      <label for="NameDemo1">Course Name<span class="mandatory">*</span></label>
                                      <input type="text" class="form-control col-md-12" maxlength="50" required>
                                          <div class="invalid-feedback">
                                            Please enter Course Name.
                                          </div>
                                    </div>
                                   <div class="col-md-4 form-group">
                                      <label for="selectDemo">Your Class<span class="mandatory">*</span></label>
                                      <select class="form-control col-md-12"  required="">
                                        <option value="">Select one</option>
                                        <option value="LKG-UKG">LKG - UKG</option>
                                        <option value="class1-2">Class 1 - 2</option>
                                        <option value="class1-2">Class 3 - 5</option>
                                      </select>
                                       <div class="invalid-feedback">
                                           Please select your class.
                                        </div>
                                    </div>
                                      <div class="form-group col-md-4">
                                      <label for="NameDemo2">Fees<span class="mandatory">*</span></label>
                                     <input  name="fees" 
                                     placeholder="" class="form-control col-md-12" maxlength="5" minlength="3" required="" type="text">
                                        <div class="invalid-feedback">
                                           Please enter a Fees!
                                        </div>    
                                    </div>
                                      <div class="form-group col-md-4">
                                      <label for="NameDemo2">Discounted Fees</label>
                                     <input  name="discounted_fees"
                                     placeholder="" class="form-control col-md-12" maxlength="5" minlength="3" type="text">    
                                    </div>

                                    <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-info book_btn">Submit</button>
                                      <a href="" class="btn btn-info book_contiunue">Cancel</a>
                                  </div>
                                  </div>
                                  </form>
                            </div>
                            </div>
                    </div>
                    <div class="tab-pane" id="elf_ma_trial_class" role="tabpanel">
                  <div class="col-md-12 targetDiv">
                      <div class="text-center">
                        <h2>Add New Trial Class</h2>
                      </div>
                      <div class="main_elf">
                        <form id="bs-validate-demo1" >
                           <div class="row"> 
                         <div class="col-md-6 form-group">
                            <label for="selectDemo">Your Course ID<span class="mandatory">*</span></label>
                            <select class="form-control col-md-12" required=""  maxlength="50" minlength="4">
                              <option value="">Select one</option>
                              <option value="cours1">PR1 - Learn to Read Phonics</option>
                              <option value="course2">PR2 - Speak Early</option>
                               <option value="cours3">PR3 - Star Reader</option>
                              <option value="course4">PR4 - Speak Up</option>
                              <option value="course5">PR5 - Fast Fluency</option>
                            </select>
                             <div class="invalid-feedback">
                                 Please select your Course ID.
                              </div>
                          </div>
                          <div class="form-group col-md-6">
                            <label for="NameDemo2">Trial Class ID<span class="mandatory">*</span></label>
                           <input  name="Trial_Id" 
                           placeholder="" class="form-control col-md-12" maxlength="20" minlength="4" required="" type="text">
                              <div class="invalid-feedback">
                                 Please enter a Trial Class ID!
                              </div>    
                          </div> 
                          <div class="col-md-12 text-center">
                          <button type="submit" class="btn btn-info book_btn">Submit</button>
                            <a href="" class="btn btn-info book_contiunue">Cancel</a>
                        </div>
                        </div>
                        </form>
                  </div>
                  </div>
                </div>
                    <div class="tab-pane" id="elf_ma_event" role="tabpanel" aria-labelledby="nav-home-tab">
                 <div class="col-md-12 targetDiv" id="div3">
                    <div class="text-center">
                      <h2>Add New Event</h2>
                    </div>
                    <div class="main_elf">
                      <form id="bs-validate-demo2 ">
                         <div class="row">
                        <div class="form-group col-md-6">
                          <label for="NameDemo2">Event ID<span class="mandatory">*</span></label>
                         <input  name="event_ID" 
                         placeholder="" class="form-control col-md-12" maxlength="20" minlength="3" required="" type="text">
                            <div class="invalid-feedback">
                               Please enter Event ID!
                            </div>    
                        </div>  
                        <div class="col-md-6 form-group">
                          <label for="NameDemo1">Event Name<span class="mandatory">*</span></label>
                          <input type="text" class="form-control col-md-12" maxlength="50" required>
                              <div class="invalid-feedback">
                                Please enter Event Name.
                              </div>
                        </div>
                       <div class="col-md-4 form-group">
                          <label for="selectDemo">Your Class<span class="mandatory">*</span></label>
                          <select class="form-control col-md-12" required="">
                            <option value="">Select one</option>
                            <option value="LKG-UKG">LKG - UKG</option>
                            <option value="class1-2">Class 1 - 2</option>
                            <option value="class1-2">Class 3 - 5</option>
                          </select>
                           <div class="invalid-feedback">
                               Please select your class.
                            </div>
                        </div>
                          <div class="form-group col-md-4">
                          <label for="NameDemo2">Fees<span class="mandatory">*</span></label>
                         <input id="get_fees" name="fees" 
                         placeholder="" class="form-control col-md-12" maxlength="5" minlength="3" required="" type="text">
                            <div class="invalid-feedback">
                               Please enter Fees!
                            </div>    
                        </div>
                          <div class="form-group col-md-4">
                          <label for="NameDemo2">Discounted Fees</label>
                         <input id="discounted_fees" name="discounted_fees"
                         placeholder="" class="form-control col-md-12" maxlength="5" minlength="" type="text">    
                        </div>

                        <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-info book_btn">Submit</button>
                          <a href="" class="btn btn-info book_contiunue">Cancel</a>
                      </div>
                      </div>
                      </form>
                </div>
                </div>
              </div>
                <div class="tab-pane" id="elf_ma_batch" role="tabpanel" aria-labelledby="nav-home-tab">
              <div class="col-md-12 targetDiv" id="div4">
                  <div class="text-center">
                    <h2>Add New Batch</h2>
                  </div>
                  <div class="main_elf">
                    <form id="bs-validate-demo3" >
                       <div class="row">
                     <div class="col-md-6 form-group">
                        <label for="selectDemo">Your Category<span class="mandatory">*</span></label>
                        <select class="form-control col-md-12" required=""  id="slct1" onchange="populate(this.id,'slct2')" >
                          <option value="">Select one</option>
                          <option value="course">Course</option>
                          <option value="trial">Trial Class</option>
                          <option value="event">Event</option>
                        </select>
                         <div class="invalid-feedback">
                             Please select your Category.
                          </div>
                      </div>
                      <div class="col-md-6 form-group">
                        <label for="selectDemo">Your Course/Trial Class/Event<span class="mandatory">*</span></label>
                        <select class="form-control col-md-12" required="" id="slct2" name="slct2">
                          <option value="">Select one</option>
                        </select>
                         <div class="invalid-feedback">
                             Please select your Course/Trial Class/Event.
                          </div>
                      </div>
                     <div class="form-group col-md-6">
                        <label for="NameDemo2">Batch ID<span class="mandatory">*</span></label>
                       <input  name="Batch_ID" 
                       placeholder="" class="form-control col-md-12" required="" type="text">
                          <div class="invalid-feedback">
                             Please enter a Batch ID!
                          </div>    
                      </div>  
                      <div class="form-group col-md-6">
                        <label for="inputDate">Batch Start Date<span class="mandatory">*</span></label>
                        <input type="input" class="form-control" id="inputDate" required="">
                          <div class="invalid-feedback">
                             Please select Batch Start Date!
                          </div> 
                      </div>
                      <div class="form-group col-md-6">
                        <label for="NameDemo2">Schedule Days</label>
                       <input  name="Schedule_days" 
                       placeholder="" class="form-control col-md-12" name="Schedule_days"  type="text" maxlength="60" minlength="6">
                 
                      </div>
                        <div class="form-group col-md-6">
                        <label for="NameDemo2">Schedule Time</label>
                       <input name="Schedule_time" 
                       placeholder="" class="form-control col-md-12" name="Schedule_time" type="text" maxlength="60" minlength="6">   
                      </div>
                        <div class="form-group col-md-6">
                        <label for="NameDemo2">Max Batch Size</label>
                       <input name="Batch_size"
                       placeholder="" class="form-control col-md-12" maxlength="2" minlength="1" type="text">    
                      </div>

                      <div class="col-md-12 text-center">
                      <button type="submit" class="btn btn-info book_btn">Submit</button>
                        <a href="" class="btn btn-info book_contiunue">Cancel</a>
                    </div>
                    </div>
                    </form>
              </div>
              </div>
            </div>
              <div class="col-md-12 targetDiv" id="div5">
                  <div class="text-center">
                    <h2>Add New User</h2>
                  </div>
                  <div class="main_elf">
                    <form id="bs-validate-demo4" >
                       <div class="row">
                    <div class="col-md-6 form-group">
                        <label for="selectDemo">Your Category<span class="mandatory">*</span></label>
                        <select class="form-control col-md-12" required=""  id="slct3" onchange="populates(this.id,'slct4')" >
                          <option value="">Select one</option>
                          <option value="courses">Course</option>
                          <option value="trials">Trial Class</option>
                          <option value="events">Event</option>
                        </select>
                         <div class="invalid-feedback">
                             Please select your Category.
                          </div>
                      </div>
                      <div class="col-md-6 form-group">
                        <label for="selectDemo">Your Course/Trial Class/Event<span class="mandatory">*</span></label>
                        <select class="form-control col-md-12" required="" id="slct4" name="slct4"  id="slct5" onchange="populateses(this.id,'slct6')">
                          <option value="">Select one</option>
                        </select>
                         <div class="invalid-feedback">
                             Please select your Course/Trial Class/Event.
                          </div>
                      </div>
                     <div class="col-md-6 form-group">
                        <label for="selectDemo">Your Batch ID<span class="mandatory">*</span></label>
                        <select class="form-control col-md-12" required="" id="slct6" name="slct6"  maxlength="20" minlength="4">
                          <option value="">Select one</option>
                        
                        </select>
                         <div class="invalid-feedback">
                             Please select your Batch ID.
                          </div>
                      </div>
                      <div class="col-md-6 form-group">
                          <label for="NameDemo1">Parent's Name<span class="mandatory">*</span></label>
                          <input type="text" class="form-control col-md-12" id="name" pattern="^[A-Za-z -.]+$" onkeyup="return nameonly(this, event)" maxlength="25" required="" maxlength="50" minlength="3">
                              <div class="invalid-feedback">
                                Please enter Parent's Name.
                              </div>
                        </div>
                        <div class="col-md-6 form-group">
                        <label for="EmailDemo">Email ID<span class="mandatory">*</span></label>
                        <input type="email" class="form-control col-md-12" id="EmailDemo" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" placeholder="" required="">
                          <div class="invalid-feedback" maxlength="50" minlength="6">
                            Please Enter a valid email address!
                          </div>  
                      </div>
                      <div class="form-group col-md-6">
                        <label for="NameDemo2">Mobile Number<span class="mandatory">*</span></label>
                        <input  name="mobile_number" pattern="[1-9]{1}[0-9]{9}" placeholder="Enter Your Phone Number" class="form-control" maxlength="15" minlength="10" required="required" type="text">
                          <div class="invalid-feedback">
                             Please enter a valid mobile number!
                          </div>    
                      </div>
                        <div class="col-md-6 form-group">
                          <label for="NameDemo1">Child's Name<span class="mandatory">*</span></label>
                          <input type="text" class="form-control col-md-12" maxlength="50" minlength="3" required="">
                              <div class="invalid-feedback">
                                Please enter child's Name.
                              </div>
                        </div>
                        <div class="col-md-4 form-group">
                        <label for="selectDemo">Your Class<span class="mandatory">*</span></label>
                        <select class="form-control col-md-12" required="" id="slct7" onchange="populated(this.id,'slct8')">
                          <option value="">Select one</option>
                          <option value="LKG">LKG - UKG</option>
                          <option value="Class1">Class 1 - 2</option>
                          <option value="Class2">Class 3 - 5</option>
                        </select>
                         <div class="invalid-feedback">
                             Please select your class.
                          </div>
                      </div>
                      <div class="col-md-6 form-group">
                  <label for="selectDemo">Your child's Reading level</label>
                  <select class="form-control col-md-12" id="slct8" name="slct8">
                    <option value="Select one">Select one</option>
                  </select>
                </div>
                <div class="col-md-6 form-group">
                  <label for="selectDemo">Your child's Speaking level</label>
                  <select class="form-control col-md-12">
                    <option value="Select One">Select one</option>
                    <option value="Hesitates to speak">Hesitates to speak</option>
                    <option value="Speaks basic English">Speaks basic English</option>
                    <option value="Speaks fluently">Speaks fluently</option>
                  </select>
                </div>
                <div class="col-md-6 form-group">
                  <label for="selectDemo">Your Status</label>
                  <select class="form-control col-md-12">
                    <option value="">Select one</option>
                    <option value="Fees">Fees</option>
                    <option value="Approved">Approved</option>
                    <option value="Paid">Paid</option>
                    <option value="Poid">Deferred Payment</option>
                  </select>
                </div>
                      <div class="col-md-12 text-center">
                      <button type="submit" class="btn btn-info book_btn">Submit</button>
                        <a href="" class="btn btn-info book_contiunue">Cancel</a>
                    </div>
                    </div>
                    </form>
              </div>
              </div>
              </div>
          </div>
     
          
        </div>
      </div>
    </div>
    
  </div>
</div>-->
  	 <div class="clearfix">&nbsp;</div>

<script type="text/javascript">
  var table;
    $(function(){
          $(document).ready(function() {
                // elf_ma_course = $('#elf_ma_course').DataTable();
                // elf_ma_trial_class = $('#elf_ma_trial_class').DataTable();
                // elf_ma_event = $('#elf_ma_event').DataTable();
                // elf_ma_batch = $('#elf_ma_batch').DataTable();
                getData('elf_ma_course');
        })
    });

</script>


  </body>
  </html>
  <?php }else
          {

            header("Location: login");
          }
    ?>