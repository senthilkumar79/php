<?php
include_once('../dbconnect.php');
$con= new db();
 $msg = null;  
 if(empty($_SESSION) && isset($_POST['login'])){  
    if(!isset($_SESSION))
        session_start();
        $username = $_POST['username'];
        $password = $_POST['password'];
        $user = $con->login($username,$password);         
        error_log(print_r($user,true));
        if (isset($user['id']) && (isset($user['role']) && $user['role'] ==1)){               
            // echo "<script>location.href='/admin/course.php'</script>";    
              header("Location: course");
              
        }else if (isset($user['id']) && (isset($user['role']) && $user['role'] ==2)) {          
            // echo "<script>location.href='/admin/reports.php'</script>";    
              header("Location: reports");
           
        } else {  
            $msg='<div class="alert alert-danger">Enter a Valid Details</div>';  
        }
 }else if(!empty($_SESSION)){
  if (isset($_SESSION['userData']['id']) && isset($_SESSION['userData']['login']) && (isset($_SESSION['userData']['role']) && $_SESSION['userData']['role'] ==1)) {         
            header("Location: course");
            // echo "<script>location.href='/admin/course.php'</script>";    
    // session_destroy();    
              
        }else if (isset($_SESSION['userData']['id']) && isset($_SESSION['userData']['login']) && (isset($_SESSION['userData']['role']) && $_SESSION['role'] ==2)) {          
          header("Location: reports");
            // echo "<script>location.href='/admin/reports.php'</script>";    
           
        }
}else{
    session_destroy();    
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>ELF - Interactive LIVE Classes for  English Learnig</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="ELF_PHONICS_LOGO.ico" type="image/icon" sizes="16x16">
  <!-- font -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Livvic:wght@600&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Fredoka+One&display=swap" rel="stylesheet">

  <!-- css start -->
  <link rel="stylesheet" type="text/css" href="/webroot/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="/webroot/css/admin.css">  
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet">  
  <!-- js start -->
   <script type="text/javascript" src="/webroot/js/jquery-3.5.1.js"></script>
   <script type="text/javascript" src="/webroot/js/bootstrap.min.js"></script>   
   
   <style type="text/css">
     #login .container #login-row #login-column #login-box {
  margin-top: 120px;
  max-width: 600px;
  /*height: 320px;*/
  border: 1px solid #9C9C9C;
  background-color: #EAEAEA;
}
#login .container #login-row #login-column #login-box #login-form {
  padding: 22px;
}
#login .container #login-row #login-column #login-box #login-form #register-link {
  margin-top: -85px;
}
.elf_logo img{
  width: 200px;
}
   </style>
</head>

<body>
    
	<!-- header ends here and Parents Chat  Start-->
  <div id="header"><?php include('../header.php');?></div>
   <div id="login">
       
        <div class="container">
            <div id="login-row" class="row justify-content-center align-items-center">

                <div id="login-column" class="col-md-6">

                    <div id="login-box" class="col-md-12">
                        
                        <form id="login-form" class="form" action="" method="post">
                          <?php echo ($msg !== null)?$msg:null; ?>
                           <div class="text-center elf_logo">
                            <img src="../webroot/img/elf_logo.png" class="img-fluid" alt="elf_logo">
                          </div>
                            <h3 class="text-center text-info">Admin Login</h3>
                            <div class="form-group">
                                <label for="username" class="text-info">Email:</label><br>
                                <input type="email" name="username" id="username" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="password" class="text-info">Password:</label><br>
                                <input type="password" name="password" id="password" class="form-control" required>
                            </div>
                            <div class="form-group text-center">
                              <button type="submit" name="login" class="btn btn-info" id="submit">Login</button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>