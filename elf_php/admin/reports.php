<?php
if(!isset($_SESSION))
  session_start();
include_once('../dbconnect.php');
include_once('../constant.php');
if(isset($_SESSION) && isset($_SESSION['userData']['id']))
{

  $con= new db();  
?>

  <!DOCTYPE html>
  <html lang="en">
  <head>
    <title>ELF - Interactive LIVE Classes for  English Learnig</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="ELF_PHONICS_LOGO.ico" type="image/icon" sizes="16x16">
    <!-- font -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Livvic:wght@600&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Fredoka+One&display=swap" rel="stylesheet">

    <!-- css start -->
    <link rel="stylesheet" type="text/css" href="/webroot/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/webroot/css/admin.css">
    <link rel="stylesheet" type="text/css" href="/webroot/css/dataTables.bootstrap4.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet">
    <!-- <link rel="stylesheet" type="text/css" href="/webroot/css/bootstrap-datepicker.css"> -->
    <!-- js start -->
     <script type="text/javascript" src="/webroot/js/jquery-3.5.1.js"></script>
     <script type="text/javascript" src="/webroot/js/bootstrap.min.js"></script>
     <link rel="stylesheet" href="/webroot/css/jquery-ui.css">
<!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
<script src="/webroot/js/jquery-ui.js"></script>
     <script type="text/javascript" src="/webroot/js/jquery.dataTables.min.js"></script>
     <script type="text/javascript" src="/webroot/js/dataTables.bootstrap4.min.js"></script>
     <!-- <script type="text/javascript" src="/webroot/js/bootstrap-datepicker.js"></script> -->
     
     
<script type="text/javascript" src="/webroot/js/admin.js"></script>  
  </head>
  <body>

  	<!-- header ends here and Parents Chat  Start-->
  <nav class="header navbar navbar-expand-md navbar-light">
        <div class="container">

          <a href="index.html" class="navbar-brand ag_logo"><img src="/webroot/img/elf_logo.png" class="img-fluid" alt="elf_logo"></a>


          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

          <div class="collapse navbar-collapse" id="navbarNav">
            <div class="navbar-nav  ml-auto">              
              <a href="course" class="nav-item nav-link">Course</a>
              <a href="logout" class="nav-item nav-link">Logout</a>
              </div>
          </div>
        </div>
      </nav>

  <section id="tabs" class="project-tab">
              <div class="container">                
                <div class="clearfix">&nbsp;</div>
                  <div class="row">
                      <div class="col-md-12 main_tab">
                          <nav>
                              <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                  <a class="nav-item nav-link active" id="nav-trial-tab" data-toggle="tab" href="#nav-trial" role="tab" aria-controls="nav-profile" aria-selected="false" onclick="getReportData('elf_tr_trial_class_booking',0);">Trial Class Registration</a>
                                  <a class="nav-item nav-link " id="nav-trial-tab" data-toggle="tab" href="#nav-ftrial" role="tab" aria-controls="nav-profile" aria-selected="false" onclick="getReportData('elf_tr_trial_booking',0);">Free Trial Registration</a>
                                  <a class="nav-item nav-link" id="nav-event-tab" data-toggle="tab" href="#nav-event" role="tab" aria-controls="nav-batch" aria-selected="false"  onclick="getReportData('elf_tr_purchase',2);">Event Registration</a>
                                  <a class="nav-item nav-link" id="nav-batch-tab" data-toggle="tab" href="#nav-batch" role="tab" aria-controls="nav-event" aria-selected="false"  onclick="getReportData('elf_tr_purchase',1);">Course Registration</a>                                  
                                  <a class="nav-item nav-link" id="nav-batch-tab" data-toggle="tab" href="#nav-payment" role="tab" aria-controls="nav-event" aria-selected="false"  onclick="getReportData('elf_tr_purchase',0);">Payment Report</a>                                  
                              </div>
                          </nav>
                          <div class="tab-content" id="nav-tabContent">
                                <div class="ajaxload"></div>
                              <div class="tab-pane fade show active" id="nav-trial" role="tabpanel" aria-labelledby="nav-home-tab">            
                                  <div class="container">
                                    <!-- <div class="">
                                      <button type="button" class="btn btn-info add_newbtn showSingle" target="1" data-href='#course-tab' data-toggle="modal" data-target="#myModal" title="add course"><i class="fa fa-plus" aria-hidden="true"></i> Add New Course</button>
                                    </div> -->
                                    <div class="clearfix">&nbsp;</div>
                                <div class="text-center">
                                  <h2>Trial Class Registration List</h2>
                                </div>
                                 <div class="col-md-12 table-responsive-lg">
                                   <table id="elf_tr_trial_class_booking_0" class="table table-striped table-bordered table-responsive parents_table" style="width:100%">
                               <thead>
                                    <tr>
                                        <th>Srno</th>
                                        <th>Batch Name</th>
                                        <th>Trial Class</th>
                                        <th>Course Id</th>
                                        <th>Course Name</th>
                                        <th>Registration Mode</th>
                                        <th>Parent Name</th>
                                        <th>Parent Email</th>
                                        <th>Parent Mobile</th>
                                        <th>Student Class</th>
                                        <th>Reading Level</th>
                                        <th>Speaking Level</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  
                                  </tbody>
                                  </table>
                              </div>
                          </div>
                        </div>
                        <div class="tab-pane fade" id="nav-ftrial" role="tabpanel" aria-labelledby="nav-profile-tab">
                            <div class="container">
                              <!-- <div class="">
                                <button type="button" class="btn btn-info add_newbtn showSingle" target="2" data-href='#trial-tab' data-toggle="modal" data-target="#myModal" title="add trial class"><i class="fa fa-plus" aria-hidden="true"></i> Add New Trial Class</button>
                               </div> -->
                               <div class="clearfix">&nbsp;</div>
                             <div class="text-center">
                                  <h2>Free Trial Registration</h2>
                            </div>
                             <div class="col-md-12 table-responsive-lg">
                               <table id="elf_tr_trial_booking_0" class="table table-striped table-bordered table-responsive parents_table col-md-12">
                           <thead>
                                <tr>
                                    <th>Srno</th>                                    
                                    <th>Course ID</th>
                                    <th>Course Name</th>
                                    <th>Parent Name</th>                                    
                                    <th>Parent Email</th>
                                    <th>Parent Mobile</th>
                                    <th>Student Class</th>
                                  </tr>
                            </thead>
                            <tbody>                         
                              </tbody>
                              </table>
                          </div>
                        </div>
                        </div>
                          <div class="tab-pane fade" id="nav-event" role="tabpanel" aria-labelledby="nav-batch">
                              <div class="container">
                                    <!-- <div class="">
                                      <button type="button" class="btn btn-info add_newbtn showSingle" target="3" data-href='#event-tab' data-toggle="modal" data-target="#myModal" title="add event"><i class="fa fa-plus" aria-hidden="true"></i> Add New Event</button>
                                    </div> -->
                                    <div class="clearfix">&nbsp;</div>
                                <div class="text-center">
                                  <h2>Event Registration List</h2>
                                </div>
                                 <div class="col-md-12 table-responsive-lg">
                                   <table id="elf_tr_purchase_2" class="table table-striped table-bordered table-responsive  parents_table" style="width:100%">
                                   <thead>
                                          <tr>
                                               <th>Srno</th>
                                              <th>Batch Id</th>
                                              <th>Event Id</th>
                                              <th>Event Name</th>                                              
                                              <th>Payment Mode</th>
                                              <th>Parent Name</th>
                                               <th>Parent Email</th>
                                               <th>Parent Mobile</th>
                                               <th>Student Name</th>
                                               <th>Student Class</th>                                              
                                          </tr>
                                      </thead>
                                      <tbody>                          
                                        </tbody>
                                        </table>
                                  </div>
                              </div>
                        </div>

                          <div class="tab-pane fade" id="nav-batch" role="tabpanel" aria-labelledby="nav-event">
                              <div class="container">
                                    <!-- <div class="">
                                      <button type="button" class="btn btn-info add_newbtn showSingle" target="4" data-href='#batch-tab' data-toggle="modal" data-target="#myModal" title="add batch"><i class="fa fa-plus" aria-hidden="true" ></i> Add New Batch</button>
                                    </div> -->
                                    <div class="clearfix">&nbsp;</div>
                                <div class="text-center">
                                  <h2>Course Registration List</h2>
                                </div>
                                 <div class="col-md-12 table-responsive-lg">
                                   <table id="elf_tr_purchase_1" class="table table-striped table-bordered table-responsive  parents_table" style="width:100%">
                                     <thead>
                                          <tr>
                                              <th>Srno</th>
                                              <th>Batch Id</th>
                                              <th>Course Id</th>
                                              <th>Course Name</th>                                              
                                              <th>Payment Mode</th>
                                              <th>Parent Name</th>
                                               <th>Parent Email</th>
                                               <th>Parent Mobile</th>
                                               <th>Student Name</th>
                                               <th>Student Class</th>                                              
                                                                                            
                                          </tr>
                                      </thead>
                                        <tbody>                     
                                        </tbody>
                                        </table>
                                  </div>
                              </div>
                          </div>
                          <div class="tab-pane fade" id="nav-payment" role="tabpanel" aria-labelledby="nav-event">
                              <div class="container">
                                    <!-- <div class="">
                                      <button type="button" class="btn btn-info add_newbtn showSingle" target="4" data-href='#batch-tab' data-toggle="modal" data-target="#myModal" title="add batch"><i class="fa fa-plus" aria-hidden="true" ></i> Add New Batch</button>
                                    </div> -->
                                    <div class="clearfix">&nbsp;</div>
                                <div class="text-center">
                                  <h2>Payment Report</h2>
                                </div>
                                 <div class="col-md-12 table-responsive-lg">
                                   <table id="elf_tr_purchase_0" class="table table-striped table-bordered table-responsive  parents_table" style="width:100%">
                                     <thead>
                                          <tr>
                                              <th>Srno</th>
                                              <th>Batch Id</th>
                                              <th>Product Id</th>
                                              <th>Product Name</th>                                              
                                              <th>Payment Mode</th>
                                              <th>Parent Name</th>
                                               <th>Parent Email</th>
                                               <th>Parent Mobile</th>
                                               <th>Student Name</th>
                                               <th>Student Class</th>                                              
                                                                                            
                                          </tr>
                                      </thead>
                                        <tbody>                     
                                        </tbody>
                                        </table>
                                  </div>
                              </div>
                          </div>
                            
  	 <div class="clearfix">&nbsp;</div>

<script type="text/javascript">
  var table;
    $(function(){
          $(document).ready(function() {
                // elf_ma_course = $('#elf_ma_course').DataTable();
                // elf_ma_trial_class = $('#elf_ma_trial_class').DataTable();
                // elf_ma_event = $('#elf_ma_event').DataTable();
                // elf_ma_batch = $('#elf_ma_batch').DataTable();
                getReportData('elf_tr_trial_class_booking',0);
        })
    });

</script>


  </body>
  </html>
  <?php }else
          {

            header("Location: login");
          }
    ?>