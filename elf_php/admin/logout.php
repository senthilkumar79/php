<?php
// Destroy entire session data
// include('login.php');
if(!isset($_SESSION))
	session_start();
include_once('../dbconnect.php');
unset($_SESSION['userData']);
// print_r($_SESSION);
session_destroy();

// Redirect to homepage
header("Location:login");
?>