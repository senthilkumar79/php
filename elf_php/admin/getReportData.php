<?php
if(!isset($_SESSION))
  session_start();
include_once('../dbconnect.php');
if(!empty($_SESSION) && isset($_SESSION['userData']['id']))
{

  $con= new db();
  if(isset($_POST['tname'])){
      $data = $con->getReport($_POST['tname'],$_POST['type']);
  	  if(!empty($data)){
  	  	 header("Content-Type:application/json");      	  
			  echo json_encode($data);			      
		}
	}
}else{

	header("Location:login");
  exit();

}

?>
