<?php
	include_once('dbconnect.php');
	$con= new db();
	$msg = null;  
	// print_r($_GET);
 	$decoded = base64_decode($_GET['link']);    
  	if(strrpos($decoded,'@') !== false)
        $decoded_details = explode("@",$decoded);        
    if(strrpos($decoded_details[0],'##') !== false)
        $decoded_array = explode("##",$decoded_details[0]);

	$url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	$isvalid = $con->validateURL($url,$decoded_array);		
	$batch_details=array();
	// print_r($isvalid);
	if(!empty($isvalid)){		
			$batch_details = $isvalid['batches'];			
	}      	
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>ELF - Interactive LIVE Classes for  English Learning</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="/ELF_PHONICS_LOGO.ico" type="image/icon" sizes="16x16">
  <!-- font -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Livvic:wght@600&display=swap" rel="stylesheet">
  <!-- css start -->
  <link rel="stylesheet" type="text/css" href="/webroot/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="/webroot/css/main.css">
   <link rel="stylesheet" type="text/css" href="/webroot/css/style.css">
   <link rel="stylesheet" type="text/css" href="/webroot/css/responsive.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet">
  <!-- js start -->
  <script type="text/javascript" src="/webroot/js/jquery-3.5.1.js"></script>
  <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="/webroot/js/script.js"></script>
   <style type="text/css">
     .bg-light {
    border-bottom: 1px solid #ccc;
}
   </style>
</head>
<body>

<?php include_once('constant.php');?>
<div id="header">
	<?php include_once('header.php');?>
</div>
    
	<!-- header ends here and Parents Chat  Start-->
	 <div class="clearfix">&nbsp;</div>
<div class="container">
  <div class="col-md-12">
    <div class="col-md-10 offset-md-1 main_elf" style="min-height: 500px;">
      <div class="goto_back1">
        
       <div class="home_newphone">
          <a href="tel:+919176692924">
        <img src="/webroot/img/icon.png" alt="whatsapp-logo" class="whatsapp-logo"> <span>+91 96001 86000</span></a>
      </div>
      </div>

      <?php if($isvalid['status'] ==200  && !empty($batch_details) && !empty($batch_details['batches'])){?>
			      <div class="elf_details text-center">
			        <img src="/webroot/img/elf_logo.png" alt="elf_logo">
			        <h2><span class="learn_elf"><?php echo $batch_details['course_name'];?></span>
			            <!-- <span class="learn_elf1">Phonics</span>--></h2>
			      <h4>Please enter your details</h4>
			    </div>
			<form id="bs-validate-demo" action="" method="post" onsubmit="return register(this,'elf_tr_trial_class_booking');">
				<input type="hidden" name="type" value="1" />
			   <div class="row">
			  <div class="col-md-6 form-group">
			    <label for="NameDemo1">Parent's Name<span class='mandatory'>*</span></label>
			    <input type="text" class="form-control col-md-12" name='parent_name' maxlength="25" pattern="^[a-zA-Z. ]+$" required>
			        <div class="invalid-feedback">
			          Please enter Parent's Name.
			        </div>
			  </div>
			   <div class="col-md-6 form-group">
			    <label for="EmailDemo">Email<span class='mandatory'>*</span></label>
			    <input type="email" class="form-control col-md-12" name='parent_email' id="EmailDemo" pattern="^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$" placeholder="" required>
			      <div class="invalid-feedback">
			        Please Enter a valid email address!
			      </div>  
			  </div>

			  <div class="form-group col-md-6">
			    <label for="NameDemo2">Mobile Number<span class='mandatory'>*</span></label>
			   <input id="edit1" name="parent_mobile" pattern="[1-9]{1}[0-9]{9}" 
			   placeholder="" class="form-control col-md-12" maxlength="10" minlength="10" required="" type="text">
			      <div class="invalid-feedback">
			         Please enter a valid mobile number!
			      </div>    
			  </div> 
			 <div class="col-md-6 form-group">
			    <label for="selectDemo">Your child's Class<span class='mandatory'>*</span></label>
			    <select class="form-control col-md-6" name='student_class' id="slct1" onchange="populate(this.id,'slct2')" required="">
			      <option value="">Select Class</option>
			      <?php foreach ($class_selection[str_replace(' ','',$batch_details['class'])] as $key => $value) {?>
			      <option value="<?php echo $value;?>"><?php if(gettype($value)== 'integer') echo 'Class '.$value;else echo $value;?></option>
			      <?php }?>

			    </select>
			     <div class="invalid-feedback">
			         Please select your child class.
			      </div>
			  </div>
			 <div class="col-md-6 form-group">
			    <label for="selectDemo">Your child's Reading level</label>
			    <select class="form-control col-md-12" name='student_reading_level' id="slct2" name="slct2">
			      <option value="">Select child's level</option>
			    </select>
			  </div>
			  <div class="col-md-6 form-group">
			    <label for="selectDemo">Your child's Speaking level</label>
			    <select class="form-control col-md-12" name='student_speaking_level'>
			      <option value="">Select child's level</option>
			      <option value="Hesitates to speak">Hesitates to speak</option>
			      <option value="Speaks basic English">Speaks basic English</option>
			      <option value="Speaks fluently">Speaks fluently</option>
			    </select>
			  </div>
			     <div class="col-md-6 offset-md-3 form-group">
			    <label for="selectDemo">Select slot for Trial Class <span class='mandatory'>*</span></label>
			    <select class="form-control col-md-12" name='trial_batch_id' id="slct1" required="">
			      <option value="">Select Your Slot</option>
			      <?php foreach ($batch_details['batches'] as $key => $batches) {?>
			      	<option value="<?php echo $batches['id'].'_'.$batches['reference_id'];?>"><?php echo $batches['batch_id'];?></option>
			      <?php }?>
			    </select>
			     <div class="invalid-feedback">
			         Please select your slot for Trial Class.
			      </div>
			  </div>
			  <div class="col-md-12 text-center">
			  <button type="submit" class="btn btn-info book_btn">Book a TRIAL Class</button>
			  <a href='/' class="btn btn-info book_contiunue">Cancel</a>
			    <!-- <a href="course_lkgukg.html" class="btn btn-info book_contiunue">Cancel</a> -->

			</div>
			</div>
			</form>
		<?php }else if($isvalid['status'] ==200  && !empty($batch_details) && empty($batch_details['batches'])){?>
				<div class="col-md-12 text-center mt-5">
				<h2>Slots not available!!!</h2>
			</div>
		<?php }else{?>
			<div class="col-md-12 text-center mt-5">
				<h2>OOPs!! Invalid Link...</h2>
			</div>
		<?php }?>	
</div>
</div>
</div>
<div class="modal" id="thank-you" data-backdrop='static'>
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <!-- <h4 class="modal-title">Thank You</h4> -->
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <h3>Thank you</h3>
        <h3>You have been registered for the trial class.</h3>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <a href='/' class="btn btn-danger">Close</a>
      </div>

    </div>
  </div>
</div>
<!-- whatsapp -->
   <a href="https://wa.me/+91 96001 86000" class="whatsapp_float whats-app" target="_blank">
    <i class="fa fa-whatsapp whatsapp-icon my-float"></i>
  </a>
<!-- footer start -->
<div id="footer"><?php include_once('footer.php');?></div>
<script>
(function() {
  'use strict';

  window.addEventListener('load', function() {
    var form = document.getElementById('bs-validate-demo');
    if($(form).length >0){
	    form.addEventListener('submit', function(event) {
	      if (form.checkValidity() === false) {
	        event.preventDefault();
	        event.stopPropagation();
	      }
	      form.classList.add('was-validated');
	    }, false);
	}
	  }, false);

})();
</script>

	 
<script>
function populate(s1,s2){
  var s1 = document.getElementById(s1);
  var s2 = document.getElementById(s2);
  s2.innerHTML = "";
  if(s1.value == ""){
    var optionArray = ["|Select child's level"];
  }else if(s1.value == "LKG" || s1.value == "UKG"){
    var optionArray = ["|Select child's level","Beginner|Beginner","Reads Letters|Reads Letters","Reads Words|Reads Words","Reads Sentences|Reads Sentences","Reads 8-10 line Stories|Reads 8-10 line Stories"];
  } else if(s1.value == "1" || s1.value == "2"){
    var optionArray = ["|Select child's level","Reads Letters|Reads Letters","Reads Words|Reads Words","Reads Sentences|Reads Sentences","Reads 8-10 line Stories|Reads 8-10 line Stories"];
  } else if(s1.value == "3" || s1.value == "4" || s1.value == "5"){
    var optionArray = ["|Select child's level","Reads Words|Reads Words","Reads Sentences|Reads Sentences","Reads 8-10 line Stories|Reads 8-10 line Stories"];
  }
  for(var option in optionArray){
    var pair = optionArray[option].split("|");
    var newOption = document.createElement("option");
    newOption.value = pair[0];
    newOption.innerHTML = pair[1];
    s2.options.add(newOption);
  }
}
</script>
</body>
</html>