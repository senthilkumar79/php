<?php
  include_once('dbconnect.php');
  $con= new db();
  $allcourses =array();
  $allcourses = $con->getAllData('elf_ma_course');
// print_r($allcourses);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>ELF - Interactive LIVE Classes for  English Learnig</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="/ELF_PHONICS_LOGO.ico" type="image/icon" sizes="16x16">
  <!-- font -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Livvic:wght@600&display=swap" rel="stylesheet">
  <!-- css start -->
  <link rel="stylesheet" type="text/css" href="webroot/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="webroot/css/main.css">
   <link rel="stylesheet" type="text/css" href="webroot/css/style.css">
   <link rel="stylesheet" type="text/css" href="webroot/css/responsive.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet">
  <!-- js start -->
  <script type="text/javascript" src="webroot/js/jquery-3.5.1.js"></script>
  <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="webroot/js/script.js"></script>

   </style>
</head>
<body>

<div id="header">
  <?php include_once('constant.php');
  include_once('header.php');?>
</div>
    
	<!-- header ends here and Parents Chat  Start-->
	 <div class="clearfix">&nbsp;</div>
   <div class="ajaxload"></div>
<div class="container">
  <div class="col-md-12">
    <div class="col-md-10 offset-md-1 main_elf">
      <div class="goto_back1">
        <!-- <a href="index.html" class=""> <i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i></a> -->
        <div class="home_newphone">
          <a href="tel:+919176692924">
        <img src="webroot/img/icon.png" alt="whatsapp-logo" class="whatsapp-logo"> <span>+91 96001 86000</span></a>
      </div>
      </div>
      <?php if(!empty($allcourses) && !empty($allcourses['data'])){?>
              <div class="elf_details text-center">
                   <img src="webroot/img/elf_logo.png" alt="elf_logo">
              <h4>Please enter your details</h4>
            </div>
        <form id="bs-validate-demo" method='POST' action='/userRegistration' onsubmit="return register(this,'elf_tr_trial_booking');">
           <div class="row">
          <div class="col-md-6 form-group">
            <label for="NameDemo1">Parent's Name<span class='mandatory'>*</span></label>
            <input type="text" class="form-control col-md-12" name='parent_name' maxlength="25" pattern="^[a-zA-Z. ]+$" required>
                <div class="invalid-feedback">
                  Please enter Parent's Name.
                </div>
          </div>
           <div class="col-md-6 form-group">
            <label for="EmailDemo">Email<span class='mandatory'>*</span></label>
            <input type="email" class="form-control col-md-12" name='parent_email' id="EmailDemo" pattern="^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$" placeholder="" required>
              <div class="invalid-feedback">
                Please Enter a valid email address!
              </div>  
          </div>

          <div class="form-group col-md-6">
            <label for="NameDemo2">Mobile Number<span class='mandatory'>*</span></label>
           <input id="edit1" name="parent_mobile" pattern="[1-9]{1}[0-9]{9}" 
           placeholder="" class="form-control col-md-12" maxlength="10" minlength="10" required="" type="text">
              <div class="invalid-feedback">
                 Please enter a valid mobile number!
              </div>    
          </div>    

         <div class="col-md-6 form-group">
            <label for="selectDemo">Your child's Class<span class='mandatory'>*</span></label>
            <select class="form-control col-md-6" name="student_class" id="slct1" required="">
              <option value="">Select child's class</option>
              <?php foreach($class_selection as $class){
                    foreach ($class as $ckey => $value) {?>                    
                        <option value="<?php echo $value;?>"><?php if(gettype($value)== 'integer') echo 'Class '.$value;else echo $value;?></option>
              <?php }}?>
            
            </select>
             <div class="invalid-feedback">
                 Please select your child class.
              </div>
          </div>
          
          <div class="col-md-6 offset-md-3 form-group">
            <label for="selectDemo">Your child’s preferred course<span class='mandatory'>*</span></label>
            <select class="form-control col-md-12" name="student_prefered_course" required>
              <option value="">Select Course</option>
              <?php foreach ($allcourses['data'] as $key => $value) {?>
                <option value="<?php echo $value['id'];?>"><?php echo $value['course_id'].' - '.$value['course_name'];?></option>  
              <?php }?>              
            </select>
          </div>
          <div class="col-md-12 text-center">
          <button type="submit" class="btn btn-info book_btn">Book a TRIAL Class</button>
          <!-- <button type="button" class="btn btn-info book_contiunue" onclick="reset(this);">Cancel</button>           -->
          <a href='/' class="btn btn-info book_contiunue">Cancel</a>

        </div>
        </div>
        </form>
      <?php }else{?>
        <div class="col-md-12">
          <h1>Registration Not Open!!!</h1>
        </div>
      <?php }?>
</div>
</div>
</div>
<!-- The Modal -->
<div class="modal" id="thank-you" data-backdrop='static'>
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <!-- <h4 class="modal-title">Thank You</h4> -->
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <h3>Thank you</h3>
        <h3>You have been registered for the trial class.</h3>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
        <a href='/' class="btn btn-danger">Close</a>
      </div>

    </div>
  </div>
</div>
<!-- whatsapp -->
   <a href="https://wa.me/+91 96001 86000" class="whatsapp_float whats-app" target="_blank">
    <i class="fa fa-whatsapp whatsapp-icon my-float"></i>
  </a>
<!-- footer start -->
<div id="footer"><?php include_once('footer.php');?></div>
<script>
(function() {
  'use strict';

  window.addEventListener('load', function() {
    var form = document.getElementById('bs-validate-demo');
    form.addEventListener('submit', function(event) {
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add('was-validated');
    }, false);
  }, false);
})();
</script>
</body>
</html>